/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.dto.CustomerDTO;
import id.co.adira.partner.tracking.entity.Customer;

/**
 * @author Vionza
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

	@Query("select o from Customer o where o.customerId = :custNo")
	Customer findByCustomerId(@Param("custNo") String customerId);

}
