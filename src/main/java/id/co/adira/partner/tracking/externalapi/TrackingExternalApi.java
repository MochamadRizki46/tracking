/**
 * 
 */
package id.co.adira.partner.tracking.externalapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author Vionza
 *
 */

@Service
public class TrackingExternalApi {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TrackingExternalApi.class);
	
	@Value("${key.api.ecm}")
	private String keyEcm;

	
	@Value("${url.api.ecm}")
	private String urlEcm;

	private RestTemplate restTemplate = new RestTemplate();

	private HttpHeaders headers = new HttpHeaders();
	
	public byte[] getDocumentEcm(String objectId, String requestId) {
		
		
		
		byte[] document = null;
		
		headers.set("x-api-key", keyEcm);
		
		try {
			HttpEntity<byte[]> entity = new HttpEntity<>(headers);

			UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(urlEcm).queryParam("objectPath", "")
					.queryParam("objectId", objectId).queryParam("requestId", requestId).queryParam("application", "A1PARTNER")
					.queryParam("objectStore", "adiraos");

			ResponseEntity<byte[]> response = restTemplate.exchange(urlBuilder.build().toUri(), HttpMethod.GET, entity,
					byte[].class);

			document = response.getBody();
		} catch (Exception e) {
			LOGGER.error("Response Error get File From ECM: {}", e.toString());
		}
		return document;
	}
}
