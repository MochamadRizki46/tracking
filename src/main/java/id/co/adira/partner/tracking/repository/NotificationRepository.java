/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import id.co.adira.partner.tracking.entity.Notification;
import id.co.adira.partner.tracking.dto.responses.NotificationResDTO;



/**
 * @author Vionza
 *
 */
@Repository
public interface NotificationRepository extends JpaRepository<Notification, String> {

	@Query("select new id.co.adira.partner.tracking.dto.responses.NotificationResDTO"
			+ "(a.notifId, "
			+ "a.orderNo, "
			+ "a.applicationNo, "
			+ "a.applicationNoUnit, "
			+ "a.title, "
			+ "a.body, "
			+ "a.status, "
			+ "a.isRead, "
			+ "to_char(a.createdDate, 'dd mon yyyy'), "
			+ "to_char(a.createdDate, 'HH24:mi'), "
			+ "a.userId) "
			+ "from Notification a where a.userId = :userId and a.active = true order by a.createdDate")
	List<NotificationResDTO> findNotificationByUserId(@Param("userId") String userId);
	
	@Transactional
	@Modifying
	@Query("update Notification n set n.isRead = true where n.notifId = :notifId")
	void updateNotifById(@Param("notifId") String notifId);
}

