package id.co.adira.partner.tracking.entity.params;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "para_kecamatan")
@Data
public class ParaKecamatan {

	@Id
	@Column(name = "para_kabkot_id")
	private String paraKabkotId;
	@Column(name = "para_kecamatan_id")
	private String paraKecamatanId;
	@Column(name = "para_kecamatan_name")
	private String paraKecamatanName;
	@Column(name = "effective_date")
	private Date effectiveDate;
	@Column(name = "active")
	private int active;
	@Column(name = "logid")
	private BigDecimal logid;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
}
