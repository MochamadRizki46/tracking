package id.co.adira.partner.tracking.entity.params;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Vionza
 *
 */

@Data
@Table(name = "para_cp_unit")
@Entity
public class ParaCpUnit {


	@Column(name = "para_cp_sentra_id")
	private String sentraId;

	@Id
	@Column(name = "para_cp_unit_id")
	private String branchId;
	
	@Column(name = "para_cp_unit_name")
	private String branchName;
	@Column(name = "para_address")
	private String address;
	@Column(name = "para_rt")
	private String rt;
	@Column(name = "para_rw")
	private String rw;
	@Column(name = "para_kelurahan_id")
	private String kelurahanId;
	@Column(name = "para_phone1_area")
	private String phoneArea1;
	@Column(name = "para_phone1")
	private String phone1;
	@Column(name = "para_phone2_area")
	private String phoneArea2;
	@Column(name = "para_phone2")
	private String phone2;
	@Column(name = "para_fax_area")
	private String faxArea;
	@Column(name = "para_fax")
	private String fax;
	@Column(name = "para_sentra_type")
	private BigDecimal sentraType;
	@Column(name = "para_ouid")
	private String ouid;
	@Column(name = "effective_date")
	private Date effectiveDate;
	@Column(name = "logid")
	private BigDecimal logid;
	@Column(name = "created_date")
	private Date createDate;
	@Column(name = "created_by")
	private String createBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "active")
	private int active;
	
}


