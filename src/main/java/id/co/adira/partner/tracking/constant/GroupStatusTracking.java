/**
 * 
 */
package id.co.adira.partner.tracking.constant;


/**
 * @author Vionza
 *
 */
public enum GroupStatusTracking {
	SVR, SVY, SCA, SPO, SPP, SOD, SRJ, SCO, DRAFT,BERLANGSUNG, MAP;

	public String mapGroupsStatus(String groupStatus) {

		var value = "";

		switch (groupStatus) {
		case "Verifikasi Order":
			value = SVR.toString();
			break;
		case "Verifikasi":
			value = SVR.name();
			break;
		case "Survey":
			value = SVY.name();
			break;
		case "Analisis Kredit":
			value = SCA.name();
			break;
		case "PO":
			value = SPO.name();
			break;
		case "Proses Penagihan":
			value = SPP.name();
			break;
		case "Order Selesai":
			value = SOD.name();
			break;
		case "Ditolak":
			value = SRJ.name();
			break;
		case "Batal":
			value = SCO.name();
			break;
		default:
			value = "No Mapping";
			break;

		}
		return value;
	}
	public int sortNumber(String groupStatus) {

		var value = 0;

		switch (groupStatus) {
		case "Verifikasi":
			value = 0;
			break;
		case "Survey":
			value = 1;
			break;
		case "Analisis Kredit":
			value = 2;
			break;
		case "PO":
			value = 3;
			break;
		case "Proses Penagihan":
			value = 4;
			break;
		case "Order Selesai":
			value = 5;
			break;
		case "Ditolak":
			value = 6;
			break;
		case "Batal":
			value = 7;
			break;
		default:
			value = 0;
			break;

		}
		return value;
	}
	
	public int sortNumberTagihan(String groupStatus) {

		var value = 0;

		switch (groupStatus) {
		case "Verifikasi Penagihan":
			value = 0;
			break;
		case "Butuh Revisi":
			value = 1;
			break;
		case "Proses Pencairan":
			value = 2;
			break;
		case "Sudah Cair":
			value = 3;
			break;
		case "Draft Tagihan":
			value = 4;
			break;
		default:
			value = 0;
			break;

		}
		return value;
	}
}
