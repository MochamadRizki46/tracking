/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import java.math.BigInteger;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vionza
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrackingOrderSummaryResDTO{
	
	@ApiModelProperty(position = 1)
	private Integer rowNum;
	
	@ApiModelProperty(position = 2)
	private String groupStatus;
	
	@ApiModelProperty(position = 3)
	private BigInteger total;

}
