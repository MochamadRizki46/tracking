package id.co.adira.partner.tracking.dto.responses;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dak
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderHistoryResDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(position = 1)
	private String applNoUnit;
	@ApiModelProperty(position = 2)
	private String orderNo;
	@ApiModelProperty(position = 3)
	private String dateStatus;
	@ApiModelProperty(position = 4)
	private String timeStatus;
	@ApiModelProperty(position = 5)
	private String status;
	@ApiModelProperty(position = 6)
	private String substatus;
	@ApiModelProperty(position = 7)
	private String kodeGroupStatus;
	@ApiModelProperty(position = 8)
	private int sequence;

}
