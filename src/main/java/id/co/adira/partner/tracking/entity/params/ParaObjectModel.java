package id.co.adira.partner.tracking.entity.params;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "para_object_model")
@Data
public class ParaObjectModel {

	@Id
	@Column(name = "para_object_brand_id")
	private String paraObjectBrandId;
	@Column(name = "para_object_type_id")
	private String paraObjectTypeId;
	@Column(name = "para_object_genre_id")
	private String paraObjectGenreId;
	@Column(name = "para_object_model_id")
	private String paraObjectModelId;
	@Column(name = "para_object_model_name")
	private String paraObjectModelName;
	@Column(name = "effective_date")
	private Date effectiveDate;
	@Column(name = "active")
	private int active;
	@Column(name = "logid")
	private BigDecimal logid;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "visible")
	private int visible;
}
