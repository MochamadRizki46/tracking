package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaKelurahan;

@Repository
public interface ParaKelurahanRepository  extends JpaRepository<ParaKelurahan, String> {

	@Query("select o from ParaKelurahan o where o.paraKelurahanId = :kelurahanId")
	ParaKelurahan findByKelurahanId(@Param("kelurahanId") String kelurahanId);

}
