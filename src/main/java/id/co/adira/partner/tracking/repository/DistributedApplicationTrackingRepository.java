/**
* 
*/
package id.co.adira.partner.tracking.repository;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Tuple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.dto.responses.TrackingOrderHistoryResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHomeResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderTagihanResDTO;
import id.co.adira.partner.tracking.entity.ApplicationTracking;

/**
 * @author Vionza
 *
 */
@Repository
public interface DistributedApplicationTrackingRepository extends JpaRepository<ApplicationTracking, String> {

	@Query("select new id.co.adira.partner.tracking.dto.responses.TrackingOrderHistoryResDTO ( "
			+ "coalesce(c.applicationNoUnit,so.applNoUnit) as applNoUnit, " //
			+ "coalesce(b.orderNo,so.orderNo) as orderNo , " //
			+ "to_char(a.datetimeStatus, 'dd MON yyyy') as dateStatus, "
			+ "to_char(a.datetimeStatus, 'hh24:mi:ss') as timeStatus, " + "coalesce(d.applStatus, a.status) as status, "
			+ "coalesce(d.applSubStatus, 'none') as substatus, "
			+ "coalesce(d.applGroupStatus, a.groupStatus) as kodeGroupStatus, " + "a.sequence) "
			+ "from ApplicationTracking a "
			+ "left join ParaApplicationStatus d on (a.status = d.applStatusId and a.groupStatus = d.applGroupStatusId and d.squad = 'PARTNER' and d.active = 0) "
			+ "left join Application b on a.fkApplicationId = b.applicationNo and b.active = true "
			+ "left join ApplicationObject c on b.skApplication = c.fkApplication and c.active = true "
			+ "left join SubmitOrder so on so.applNoPayung = a.fkApplicationId " + "where a.sequence in "
			+ "(select max(at.sequence) " + "from ApplicationTracking at, " + "ParaApplicationStatus pas "
			+ "where at.status = pas.applStatusId " + "and at.groupStatus = pas.applGroupStatusId "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0 " + "and at.fkApplicationId = :applNoPayung "
			+ "group by pas.applGroupStatus, pas.applStatus) " + "and a.fkApplicationId =  :applNoPayung ") //
	List<TrackingOrderHistoryResDTO> findByApplUnitNo(@Param("applNoPayung") String applNoPayung);
	
	@Query("select new id.co.adira.partner.tracking.dto.responses.TrackingOrderHistoryResDTO ( "
			+ "coalesce(c.applicationNoUnit,so.applNoUnit) as applNoUnit, " 
			+ "coalesce(b.orderNo,so.orderNo) as orderNo , " 
			+ "to_char(a.datetimeStatus, 'dd MON yyyy') as dateStatus, "
			+ "to_char(a.datetimeStatus, 'hh24:mi:ss') as timeStatus, " + "coalesce(d.applStatus, a.status) as status, "
			+ "coalesce(d.applSubStatus, 'none') as substatus, "
			+ "coalesce(d.applGroupStatus, a.groupStatus) as kodeGroupStatus, " + "a.sequence) "
			+ "from ApplicationTracking a "
			+ "left join ParaApplicationStatus d on (a.status = d.applStatusId and a.groupStatus = d.applGroupStatusId and d.squad = 'PARTNER' and d.active = 0) "
			+ "left join Application b on a.fkApplicationId = b.applicationNo and b.active = true "
			+ "left join ApplicationObject c on b.skApplication = c.fkApplication and c.active = true "
			+ "left join SubmitOrder so on so.orderNo = a.processType " 
			+ "where a.sequence in "
			+ "(select max(at.sequence) " + "from ApplicationTracking at, " + "ParaApplicationStatus pas "
			+ "where at.status = pas.applStatusId " + "and at.groupStatus = pas.applGroupStatusId "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0 " 
			+ "and at.processType = :orderNo "
			+ "group by pas.applGroupStatus, pas.applStatus) " 
			+ "and a.processType = :orderNo") 
	List<TrackingOrderHistoryResDTO> findByOrderNo(@Param("orderNo") String orderNo);

	@Query("select new id.co.adira.partner.tracking.dto.responses.TrackingOrderHomeResDTO(" + "a.orderNo as orderNo, "
			+ "a.applicationNo as applNo, " + "b.applicationNoUnit as applNoUnit, "
			+ "coalesce(d.identityFullName, g.customerName) as custName, "
			+ "to_char(a.orderDate, 'dd mon yyyy') as orderDate, "
			+ "coalesce(e.applStatus, a.applicationLastStatus) as status, "
			+ "coalesce(h.objectDesc, 'MOTOR') as objectDesc, "
			+ "coalesce(h.objectModelDesc,'Menunggu Parameter') as objectModelDesc, "
			+ "coalesce(e.applGroupStatus, a.applicationLastGroupStatus) as groupStatus, "
			+ "(select cp.branchName from ParaCpUnit cp where cp.branchId = a.branchSales and cp.active = 0) as branchName, "
			+ "'' as skPo " + ") " + "from Application a left join Customer c on (a.fkCustomer = c.customerId) "
			+ "left join CustomerPersonal d on (c.skCustomer = d.fkCustomer) "
			+ "left join ParaApplicationStatus e on (a.applicationLastStatus = e.applStatusId and a.applicationLastGroupStatus = e.applGroupStatusId and e.squad = 'PARTNER' and e.active = 0), "
			+ " ApplicationObject b, SubmitOrder f "
			+ "left join  SubmitCustomer g on f.submitOrderId = g.submitOrderId "
			+ "left join SubmitUnit h on f.submitOrderId = h.submitOrderId "
			+ "where a.skApplication = b.fkApplication and a.applicationNo = f.applNoPayung and f.pic = :userId "
			+ "and to_date(to_char(a.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:fillterDate, 'dd-mm-yyyy') and coalesce(e.applStatus, a.applicationLastStatus) is not null")
	List<TrackingOrderHomeResDTO> findByDateAndUserid(@Param("fillterDate") String fillterDate, String userId);

	@Query("select new id.co.adira.partner.tracking.dto.responses.TrackingOrderHomeResDTO(" + "a.orderNo as orderNo, "
			+ "a.applicationNo as applNo, " + "b.applicationNoUnit as applNoUnit, "
			+ "coalesce(d.identityFullName, g.customerName) as custName, "
			+ "to_char(a.orderDate, 'dd mon yyyy') as orderDate, "
			+ "coalesce(e.applStatus, a.applicationLastStatus) as status, "
			+ "coalesce(h.objectDesc, 'MOTOR') as objectDesc, "
			+ "coalesce(h.objectModelDesc,'Menunggu Parameter') as objectModelDesc, "
			+ "coalesce(e.applGroupStatus, a.applicationLastGroupStatus) as groupStatus, "
			+ "(select cp.branchName from ParaCpUnit cp where cp.branchId = a.branchSales and cp.active = 0) as branchName, "
			+ "coalesce(po.skPo, '') as skPo " + ") "
			+ "from Application a left join Customer c on (a.fkCustomer = c.customerId) "
			+ "left join CustomerPersonal d on (c.skCustomer = d.fkCustomer) "
			+ "left join ParaApplicationStatus e on (a.applicationLastStatus = e.applStatusId and a.applicationLastGroupStatus = e.applGroupStatusId and e.squad = 'PARTNER' and e.active = 0), "
			+ "ApplicationObject b, SubmitOrder f "
			+ "left join  SubmitCustomer g on f.submitOrderId = g.submitOrderId left join SubmitUnit h on (f.submitOrderId = h.submitOrderId)  "
			+ "left join PurchaseOrder po on a.skApplication = po.fkApplication and po.active = 0 "
			+ " where a.skApplication = b.fkApplication " + "and a.applicationNo = f.applNoPayung "
			+ "and b.orderSourceName = :dlc "
			+ "and to_date(to_char(a.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:fillterDate, 'dd-mm-yyyy')")
	List<TrackingOrderHomeResDTO> findByDateAndDlc(@Param("fillterDate") String fillterDate, @Param("dlc") String dlc);

	@Query(nativeQuery = true, value = "select a.order_no as orderNo, " + "a.application_no as applNo, "
			+ "ao.application_no_unit as applNoUnit, "
			+ "coalesce(case when c.customer_type = 'PER' then cp.identity_full_name else cc.company_name end, toc.customer_name) as custName, "
			+ "to_char(a.order_date,'dd mon yyyy') as orderDate, " + "coalesce(pas.level2, at.status) as status, "
			+ "(select po.para_object_name from para_object po where po.para_object_id = ao.\"object\" and active = 0 ) as objectDesc, "
			+ "case when ao.\"object\" <> '014' then (select pom.para_object_model_name from para_object_model pom where pom.para_object_model_id = ao.object_model and active = 0 ) else '-' end as objectModelDesc, "
			+ "pas.level1 as groupStatus, "
			+ "(select pcu.para_cp_unit_name from para_cp_unit pcu where pcu.para_cp_unit_id = a.branch_sales and pcu.active = 0) branchName, "
			+ "'' skPo " + "from application a "
			+ "inner join application_object ao on ao.fk_application = a.sk_application  "
			+ "inner join application_tracking at on at.fk_application_id = a.application_no  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = a.application_no) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "left join customer c on customer_id = a.fk_customer "
			+ "left join customer_personal cp on cp.fk_customer = c.sk_customer  "
			+ "left join customer_company cc on cc.fk_customer = c.sk_customer  "
			+ "left join tbl_submit_order tso on tso.appl_no_payung =  a.application_no "
			+ "left join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "where a.created_by = :userId "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
//			+ "and a.active = true "
			+ "and pas.level1 = :groupStatus " + "union all " + "select tso.order_no , " + "tso.appl_no_payung , "
			+ "tso.appl_no_unit , " + "toc.customer_name , " + "to_char(tso.order_date,'dd mon yyyy') order_date, "
			+ "pas.level2 as status, " + "pobj.para_object_name , "
			+ "case when tou.object <> '014' then tou.object_model_desc else '-' end object_model_desc, "
			+ "pas.level1 as group_status , " + "'' as cabang, " + "'' as sk_po " + "from tbl_submit_order tso "
			+ "inner join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "inner join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id  "
			+ "inner join para_object pobj on pobj.para_object_id = tou.\"object\" and pobj.active = 0 "
			+ "inner join application_tracking at on at.fk_application_id = tso.appl_no_payung  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = tso.appl_no_payung ) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "where tso.status = case when :groupStatus = 'Verifikasi' then 'Order Terkirim' else '-' end "
			+ "and tso.pic = :userId " + "and tso.active = 0 "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and not exists (select 1 from application a2 where a2.application_no = tso.appl_no_payung) "
			+ "order by orderDate desc ")
	List<Tuple> findByRangeDateAndUserId(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("userId") String userId, @Param("groupStatus") String groupStatus);

	@Query(nativeQuery = true, value = "select a.order_no as orderNo, " + "a.application_no as applNo, "
			+ "ao.application_no_unit as applNoUnit, "
			+ "coalesce(case when c.customer_type = 'PER' then cp.identity_full_name else cc.company_name end, toc.customer_name) as custName, "
			+ "to_char(a.order_date,'dd mon yyyy') as orderDate, " + "coalesce(pas.level2, at.status) as status, "
			+ "(select po.para_object_name from para_object po where po.para_object_id = ao.\"object\" and active = 0 ) as objectDesc, "
			+ "case when ao.\"object\" <> '014' then (select pom.para_object_model_name from para_object_model pom where pom.para_object_model_id = ao.object_model and active = 0 ) else '-' end as objectModelDesc, "
			+ "pas.level1 as groupStatus, "
			+ "(select pcu.para_cp_unit_name from para_cp_unit pcu where pcu.para_cp_unit_id = a.branch_sales and pcu.active = 0) branchName, "
			+ "p.sk_po as skPo " + "from application a "
			+ "inner join application_object ao on ao.fk_application = a.sk_application  "
			+ "inner join application_tracking at on at.fk_application_id = a.application_no  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = a.application_no) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "left join customer c on customer_id = a.fk_customer "
			+ "left join customer_personal cp on cp.fk_customer = c.sk_customer  "
			+ "left join customer_company cc on cc.fk_customer = c.sk_customer  "
			+ "left join po p on p.fk_application = a.sk_application  "
			+ "left join tbl_submit_order tso on tso.appl_no_payung =  a.application_no "
			+ "left join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "where case when :userType = '2' and ao.third_party_id <> '' then ao.third_party_id else ao.order_source_name end = :dlc "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
//			+ "and a.active = true "
			+ "and pas.level1 = :groupStatus " + "union all " + "select tso.order_no , " + "tso.appl_no_payung , "
			+ "tso.appl_no_unit , " + "toc.customer_name , " + "to_char(tso.order_date,'dd mon yyyy') order_date, "
			+ "pas.level2 as status, " + "pobj.para_object_name , "
			+ "case when tou.object <> '014' then tou.object_model_desc else '-' end object_model_desc, "
			+ "pas.level1 as group_status , " + "'' as cabang, " + "'' as sk_po " + "from tbl_submit_order tso "
			+ "inner join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "inner join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id  "
			+ "inner join para_object pobj on pobj.para_object_id = tou.\"object\" and pobj.active = 0 "
			+ "inner join application_tracking at on at.fk_application_id = tso.appl_no_payung  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = tso.appl_no_payung ) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "inner join para_user_detail pud on upper(pud.userid) = upper(tso.pic) and pud.isactive = 0 "
			+ "where tso.status = case when :groupStatus = 'Verifikasi' then 'Order Terkirim' else '-' end  "
			+ "and tso.active = 0" + "and pud.partner_code = :dlc "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and not exists (select 1 from application a2 where a2.application_no = tso.appl_no_payung) "
			+ "order by orderDate desc ")
	List<Tuple> findByRangeDateAndDlc(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("dlc") String dlc, @Param("groupStatus") String groupStatus, @Param("userType") String userType);

	@Query("select new id.co.adira.partner.tracking.dto.responses.TrackingOrderTagihanResDTO("
			+ "to_char(a.orderDate, 'dd mon yyyy') as orderDate, " + "b.applicationNoUnit as applNoUnit, "
			+ "(select cp.branchName from ParaCpUnit cp where cp.branchId = a.branchSales and cp.active = 0) as branchId, "
			+ "coalesce(coalesce(d.identityFullName,cc.companyName), '-') as custName, "
			+ "(select pom.paraObjectModelName from ParaObjectModel pom where pom.paraObjectModelId = b.objectModel) as objectModelDesc, "
			+ "coalesce(po.poAmount, 0) as totalAmount, "
			+ "(select coalesce(to_char(max(at.datetimeStatus), 'dd mon yyyy'), to_char(current_date, 'dd mon yyyy')) from ApplicationTracking at where at.fkApplicationId = a.applicationNo and at.groupStatus = a.applicationLastGroupStatus and at.active = true) as statusDate, "
			+ "coalesce(e.applStatus, at.status) as status, "
			+ "coalesce(e.applGroupStatus, a.applicationLastGroupStatus) as groupStatus, "
			+ "coalesce(po.skPo, '') as skPo " + ") " + "from Application a "
			+ "left join Customer c on (a.fkCustomer = c.customerId) "
			+ "left join CustomerPersonal d on (c.skCustomer = d.fkCustomer) "
			+ "left join CustomerCompany cc on (c.skCustomer = cc.fkCustomer) "
			// + "inner join ParaApplicationStatus e on (a.applicationLastStatus =
			// e.applStatusId and a.applicationLastGroupStatus = e.applGroupStatusId and
			// e.applGroupStatus = 'Proses Penagihan' and e.squad = 'PARTNER' and e.active =
			// 0), "
			+ "left join ApplicationTracking at on (at.fkApplicationId = a.applicationNo and at.sequence = (select max(at2.sequence) from ApplicationTracking at2 where at2.fkApplicationId = a.applicationNo)) "
			+ "left join ParaApplicationStatus e on (at.status = e.applStatusId and at.groupStatus = e.applGroupStatusId and e.applGroupStatus = 'Proses Penagihan' and e.squad = 'PARTNER' and e.active = 0), "
			+ "ApplicationObject b "
			+ "left join PurchaseOrder po on (a.skApplication = po.fkApplication and po.active = 0) "
			+ "where a.skApplication = b.fkApplication " + "and b.thirdPartyId = :dlc "
			+ "and e.applStatus = :groupStatus "
			+ "and to_date(to_char(a.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy')")
	List<TrackingOrderTagihanResDTO> findTagihanByDateAndDlc(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("dlc") String dlc, @Param("groupStatus") String groupStatus);

	@Query(nativeQuery = true, value = " select pas.level1 as groupStatus, " + "coalesce(list.total,0) as total "
			+ "from para_appl_status pas " + "left join ( " + "select level1, count(order_no) as total " + "from ( "
			+ "select " + "pas.level1, " + "a.order_no " + "from " + "application a "
			+ "inner join application_tracking at2 on " + "(at2.fk_application_id = a.application_no "
			+ "and at2.sequence = ( " + "select " + "max(at3.sequence) " + "from " + "application_tracking at3 "
			+ "where " + "at3.fk_application_id = a.application_no)) " + "inner join para_appl_status pas on "
			+ "(at2.status = pas.para_appl_status_id " + "and at2.group_status = pas.para_appl_status_group_id "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0) " + "where "
			+ "to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and a.created_by = :userId "
//					+ "and a.active = true "
			+ "union all " + "select " + "pas.level1 , " + "tso.order_no " + "from " + "tbl_submit_order tso "
			+ "inner join application_tracking at2 on " + "(at2.fk_application_id = tso.appl_no_payung "
			+ "and at2.sequence = ( " + "select " + "max(at3.sequence) " + "from " + "application_tracking at3 "
			+ "where " + "at3.fk_application_id = tso.appl_no_payung)) " + "inner join para_appl_status pas on "
			+ "(at2.status = pas.para_appl_status_id " + "and at2.group_status = pas.para_appl_status_group_id "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0) " + "where "
			+ "to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and tso.pic = :userId " + "and tso.active = 0 " + "and tso.status = 'Order Terkirim' "
			+ "and not exists ( " + "select " + "1 " + "from " + "application a2 " + "where "
			+ "a2.application_no = tso.appl_no_payung)) fin " + "group by " + "fin.level1) list on "
			+ "pas.level1 = list.level1 " + "where " + "pas.squad = 'PARTNER' " + "group by " + "pas.level1, "
			+ "list.total")
	List<Tuple> findSummaryTrackingOrderSales(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("userId") String userId);

	@Query(nativeQuery = true, value = " select pas.level1 as groupStatus, " + "coalesce(list.total,0) as total "
			+ "from para_appl_status pas " + "left join ( " + "select level1, count(order_no) as total " + "from ( "
			+ "select " + "pas.level1, " + "a.order_no " + "from " + "application a "
			+ "inner join application_tracking at2 on " + "(at2.fk_application_id = a.application_no "
			+ "and at2.sequence = ( " + "select " + "max(at3.sequence) " + "from " + "application_tracking at3 "
			+ "where " + "at3.fk_application_id = a.application_no)) " + "inner join para_appl_status pas on "
			+ "(at2.status = pas.para_appl_status_id " + "and at2.group_status = pas.para_appl_status_group_id "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0) "
			+ "inner join application_object ao on ao.fk_application = a.sk_application  " + "where "
			+ "to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and case when :userType = '2' and ao.third_party_id <> '' then ao.third_party_id else ao.order_source_name end = :dlc "
//			+ "and a.active = true "
			+ "union all " + "select " + "pas.level1 , " + "tso.order_no " + "from " + "tbl_submit_order tso "
			+ "inner join application_tracking at2 on " + "(at2.fk_application_id = tso.appl_no_payung "
			+ "and at2.sequence = ( " + "select " + "max(at3.sequence) " + "from " + "application_tracking at3 "
			+ "where " + "at3.fk_application_id = tso.appl_no_payung)) " + "inner join para_appl_status pas on "
			+ "(at2.status = pas.para_appl_status_id " + "and at2.group_status = pas.para_appl_status_group_id "
			+ "and pas.squad = 'PARTNER' " + "and pas.active = 0) "
			+ "inner join para_user_detail pud on pud.userid = tso.pic and  pud.isactive = 0  " + "where "
			+ "to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and pud.partner_code = :dlc " + "and tso.active = 0 " + "and tso.status = 'Order Terkirim' "
			+ "and not exists ( " + "select " + "1 " + "from " + "application a2 " + "where "
			+ "a2.application_no = tso.appl_no_payung)) fin " + "group by " + "fin.level1) list on "
			+ "pas.level1 = list.level1 " + "where " + "pas.squad = 'PARTNER' " + "group by " + "pas.level1, "
			+ "list.total")
	List<Tuple> findSummaryTrackingOrderOwner(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("dlc") String dlc, @Param("userType") String userType);

	@Query("select  count(so) " + "FROM SubmitOrder so " + "WHERE "
			+ "to_date(to_char(so.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(so.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "AND upper(so.status) = 'DRAFT' " + "AND UPPER(so.pic) = UPPER(:userId) " + "AND so.active = 0 ")
	BigInteger findSummaryDraftByDateAndUserId(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("userId") String userId);

	@Query("select  count(so) " + "FROM SubmitOrder so, UserDetail ud " + "WHERE "
			+ "to_date(to_char(so.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "AND to_date(to_char(so.orderDate, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "AND upper(so.status) = 'DRAFT' " + "AND UPPER(so.pic) = UPPER(ud.userId) " + "AND ud.partnerCode = :dlc "
			+ "AND so.active = 0 " + "AND ud.active = 0")
	BigInteger findSummaryDraftByDateAndDlc(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("dlc") String dlc);

	@Query(nativeQuery = true, value = " select tso.order_no as orderNo, " + "tso.appl_no_payung as applNo, "
			+ "tso.appl_no_unit as applNoUnit, " + "toc.customer_name as custName, "
			+ "to_char(tso.order_date , 'dd mon yyyy') as orderDate, " + "tso.status , "
			+ "tou.object_desc as objectDesc, " + "tou.object_model_desc as objectModelDesc, "
			+ "upper(tso.status) groupStatus, " + "'' as branchName, " + "'' as skPo " + "from tbl_submit_order tso  "
			+ "inner join tbl_order_customer toc on toc.submit_order_id = tso.submit_order_id "
			+ "left join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id "
			+ "where upper(tso.status) = 'DRAFT' " + "and upper(tso.pic) = upper(:userId) " + "and tso.active = 0 "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') ")
	List<Tuple> findDetailOrderDraftByDateAndUserId(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("userId") String userId);

	@Query(nativeQuery = true, value = " select tso.order_no as orderNo, " + "tso.appl_no_payung as applNo, "
			+ "tso.appl_no_unit as applNoUnit, " + "toc.customer_name as custName, "
			+ "to_char(tso.order_date , 'dd mon yyyy') as orderDate, " + "tso.status , "
			+ "tou.object_desc as objectDesc, " + "tou.object_model_desc as objectModelDesc, "
			+ "upper(tso.status) groupStatus, " + "'' as branchName, " + "'' as skPo " + "from tbl_submit_order tso  "
			+ "inner join tbl_order_customer toc on toc.submit_order_id = tso.submit_order_id "
			+ "left join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id "
			+ "inner join para_user_detail pud on upper(pud.userid) = upper(tso.pic) and tso.active = 0 and pud.usertype not in ('SLD','SLM') "
			+ "where upper(tso.status) = 'DRAFT' " + "and pud.partner_code = :dlc " + "and tso.active = 0 "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date , 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') ")
	List<Tuple> findDetailOrderDraftByDateAndDlc(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("dlc") String dlc);

	@Query(nativeQuery = true, value = " select " + "pas.level2 as groupStatus, " + "(case "
			+ "when list.total is null then 0 " + "else list.total " + "end) as total " + "from "
			+ "para_appl_status pas " + "left join ( " + "select " + "pas.level2, " + "count(pas.level2) as total "
			+ "from " + "application a " + "left join application_tracking at2 on "
			+ "		(at2.fk_application_id = a.application_no " + "			and at2.sequence = ( " + "			select "
			+ "				max(at3.sequence) " + "			from " + "				application_tracking at3 "
			+ "			where " + "				at3.fk_application_id = a.application_no)) "
			+ "	left join para_appl_status pas on " + "		(at2.status = pas.para_appl_status_id "
			+ "			and at2.group_status = pas.para_appl_status_group_id "
			+ "			and pas.level1 = 'Proses Penagihan' " + "			and pas.squad = 'PARTNER' "
			+ "			and pas.active = 0), " + "application_object ao "
//			+ "para_appl_status pas "
			+ "where "
			+ "to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and a.sk_application = ao.fk_application " + "and ao.third_party_id = :dlc "
//			+ "and a.application_last_status = pas.para_appl_status_id "
//			+ "and a.application_last_group_status = pas.para_appl_status_group_id "
//			+ "and pas.squad = 'PARTNER' "
//			
//			+ "and pas.active = 0 "
			+ "and a.active = true " + "and ao.active = true " + "group by " + "pas.level2) list on "
			+ "pas.level2 = list.level2 " + "where " + "pas.squad = 'PARTNER' "
			+ "and pas.level1 in ('Proses Penagihan', 'Order Selesai') " + "group by " + "pas.level2, list.total")
	List<Tuple> findSummaryTagihan(@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("dlc") String dlc);

	@Query(nativeQuery = true, value = "select a.order_no as orderNo, " + "a.application_no as applNo, "
			+ "ao.application_no_unit as applNoUnit, "
			+ "coalesce(case when c.customer_type = 'PER' then cp.identity_full_name else cc.company_name end, toc.customer_name) as custName, "
			+ "to_char(a.order_date,'dd mon yyyy') as orderDate, " + "coalesce(pas.level2, at.status) as status, "
			+ "(select po.para_object_name from para_object po where po.para_object_id = ao.\"object\" and active = 0 ) as objectDesc, "
			+ "case when ao.\"object\" <> '014' then (select pom.para_object_model_name from para_object_model pom where pom.para_object_model_id = ao.object_model and active = 0 ) else '-' end as objectModelDesc, "
			+ "pas.level1 as groupStatus, "
			+ "(select pcu.para_cp_unit_name from para_cp_unit pcu where pcu.para_cp_unit_id = a.branch_sales and pcu.active = 0) branchName, "
			+ "'' skPo " + "from application a "
			+ "inner join application_object ao on ao.fk_application = a.sk_application  "
			+ "inner join application_tracking at on at.fk_application_id = a.application_no  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = a.application_no) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "left join customer c on customer_id = a.fk_customer "
			+ "left join customer_personal cp on cp.fk_customer = c.sk_customer  "
			+ "left join customer_company cc on cc.fk_customer = c.sk_customer  "
			+ "left join tbl_submit_order tso on tso.appl_no_payung =  a.application_no "
			+ "left join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "where a.created_by = :userId "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and a.active = true " + "and pas.level1 in ('Verifikasi','Survey','Analisis Kredit') " + "union all "
			+ "select tso.order_no , " + "tso.appl_no_payung , " + "tso.appl_no_unit , " + "toc.customer_name , "
			+ "to_char(tso.order_date,'dd mon yyyy') order_date, " + "pas.level2 as status, "
			+ "pobj.para_object_name , "
			+ "case when tou.object <> '014' then tou.object_model_desc else '-' end object_model_desc, "
			+ "pas.level1 as group_status , " + "'' as cabang, " + "'' as sk_po " + "from tbl_submit_order tso "
			+ "inner join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "inner join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id  "
			+ "inner join para_object pobj on pobj.para_object_id = tou.\"object\" and pobj.active = 0 "
			+ "inner join application_tracking at on at.fk_application_id = tso.appl_no_payung  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = tso.appl_no_payung ) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "where tso.status = 'Order Terkirim' " + "and tso.active = 0" + "and tso.pic = :userId "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and not exists (select 1 from application a2 where a2.application_no = tso.appl_no_payung) "
			+ "order by orderDate desc ")
	List<Tuple> findByRangeDateAndUserIdFillterByOrderBerlangsung(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("userId") String userId);

	@Query(nativeQuery = true, value = "select a.order_no as orderNo, " + "a.application_no as applNo, "
			+ "ao.application_no_unit as applNoUnit, "
			+ "coalesce(case when c.customer_type = 'PER' then cp.identity_full_name else cc.company_name end, toc.customer_name) as custName, "
			+ "to_char(a.order_date,'dd mon yyyy') as orderDate, " + "coalesce(pas.level2, at.status) as status, "
			+ "(select po.para_object_name from para_object po where po.para_object_id = ao.\"object\" and active = 0 ) as objectDesc, "
			+ "case when ao.\"object\" <> '014' then (select pom.para_object_model_name from para_object_model pom where pom.para_object_model_id = ao.object_model and active = 0 ) else '-' end as objectModelDesc, "
			+ "pas.level1 as groupStatus, "
			+ "(select pcu.para_cp_unit_name from para_cp_unit pcu where pcu.para_cp_unit_id = a.branch_sales and pcu.active = 0) branchName, "
			+ "'' skPo " + "from application a "
			+ "inner join application_object ao on ao.fk_application = a.sk_application  "
			+ "inner join application_tracking at on at.fk_application_id = a.application_no  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = a.application_no) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "left join customer c on customer_id = a.fk_customer "
			+ "left join customer_personal cp on cp.fk_customer = c.sk_customer  "
			+ "left join customer_company cc on cc.fk_customer = c.sk_customer  "
			+ "left join tbl_submit_order tso on tso.appl_no_payung =  a.application_no "
			+ "left join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "where case when :userType = '2' and ao.third_party_id <> '' then ao.third_party_id else ao.order_source_name end = :dlc "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(a.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and a.active = true " + "and pas.level1 in ('Verifikasi','Survey','Analisis Kredit') " + "union all "
			+ "select tso.order_no , " + "tso.appl_no_payung , " + "tso.appl_no_unit , " + "toc.customer_name , "
			+ "to_char(tso.order_date,'dd mon yyyy') order_date, " + "pas.level2 as status, "
			+ "pobj.para_object_name , "
			+ "case when tou.object <> '014' then tou.object_model_desc else '-' end object_model_desc, "
			+ "pas.level1 as group_status , " + "'' as cabang, " + "'' as sk_po " + "from tbl_submit_order tso "
			+ "inner join tbl_order_customer toc on toc.submit_order_id =tso.submit_order_id "
			+ "inner join tbl_order_unit tou on tou.submit_order_id =tso.submit_order_id  "
			+ "inner join para_object pobj on pobj.para_object_id = tou.\"object\" and pobj.active = 0 "
			+ "inner join application_tracking at on at.fk_application_id = tso.appl_no_payung  "
			+ "and at.\"sequence\" = (select max(at2.\"sequence\") from application_tracking at2 where at2.fk_application_id = tso.appl_no_payung ) "
			+ "inner join para_appl_status pas on pas.para_appl_status_id = at.status and pas.para_appl_status_group_id = at.group_status and pas.active = 0 and pas.squad = 'PARTNER' "
			+ "inner join para_user_detail pud on upper(pud.userid) = upper(tso.pic) and pud.isactive = 0 "
			+ "where tso.status = 'Order Terkirim' " + "and tso.active = 0" + "and pud.partner_code = :dlc "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(tso.order_date, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "and not exists (select 1 from application a2 where a2.application_no = tso.appl_no_payung) "
			+ "order by orderDate desc ")
	List<Tuple> findByRangeDateAndDlcFillterByOrderBerlangsung(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("dlc") String dlc, @Param("userType") String userType);
}
