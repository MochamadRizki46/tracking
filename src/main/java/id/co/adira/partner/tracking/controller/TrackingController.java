package id.co.adira.partner.tracking.controller;

import id.co.adira.partner.tracking.constant.StatusConst;
import id.co.adira.partner.tracking.dto.request.*;
import id.co.adira.partner.tracking.dto.responses.*;
import id.co.adira.partner.tracking.service.TrackingService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
public class TrackingController {

	@Autowired
	TrackingService trackingService;

	String dataNotFound = "Data Not Found";

	private static final Logger LOGGER = LoggerFactory.getLogger(TrackingController.class);

	@PostMapping("/trackingheader")
	@ApiOperation(value = "Get Tracking Order Header", nickname = "Get Tracking Order Header")
	public ResponseEntity<Object> getTrackingOrderHeader(@RequestBody TrackingOrderHeaderReqDTO getTrackingHeaderReq) {

		LOGGER.info("tracking header");
		var status = 1;
		var msg = "OK";
		var response = new TrackingOrderHeaderResDTO();

		try {
			response = trackingService.getTrackingOrderHeader(getTrackingHeaderReq);
		} catch (Exception e) {
			LOGGER.error("tracking header failed {}", e.getMessage());
		}

		if (response.getApplNoUnit() == null) {
			status = 0;
			msg = dataNotFound;
		}
		return ResponseEntity.ok(new ResponseDTO<>(status, msg, response));
	}

	@PostMapping("/trackinghistory")
	@ApiOperation(value = "Get Tracking Order History", nickname = "Get Tracking Order History")
	public ResponseEntity<Object> getTrackingOrderHistory(@RequestBody TrackingOrderHistoryReqDTO getTrackingHistReq) {

		LOGGER.info("tracking history");
		var status = 1;
		var msg = "OK";
		List<TrackingOrderHistoryResDTO> response;
		response = getTrackingHistReq.getTypeUser().equalsIgnoreCase("1")
				? trackingService.getTrackingOrderHistorySls(getTrackingHistReq)
				: trackingService.getTrackingOrderHistoryOwn(getTrackingHistReq);

		if (response.isEmpty()) {
			status = 0;
			msg = dataNotFound;
		}
		return ResponseEntity.ok(new ResponseDTO<>(status, msg, response));

	}

//	@ApiOperation(value = "getTrackingOrderHome", nickname = "getTrackingOrderHome")
//	@PostMapping("/trackingorderhome")
//	public ResponseEntity<Object> getTrackingOrderHome(@RequestBody TrackingOrderListReqDTO trackingOrderReq) {
//		List<TrackingOrderHomeResDTO> dataHome = new ArrayList<>();
//		try {
//			if (trackingOrderReq.getUserType().equalsIgnoreCase("1"))
//				dataHome = trackingService.getListTrackingHomeSls(trackingOrderReq);
//			else
//				dataHome = trackingService.getListTrackingHomeOwn(trackingOrderReq);
//
//			LOGGER.info("tracking home success");
//			return new ResponseEntity<>(dataHome, HttpStatus.OK);
//
//		} catch (Exception e) {
//			LOGGER.error("tracking home failed {}", e.getMessage());
//			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
//		}
//	}

	@PostMapping("/trackingorderdetail")
	@ApiOperation(value = "Get Tracking Order Detail", nickname = "Get Tracking Order Detail")
	public ResponseEntity<Object> getTrackingOrderDetail(
			@RequestBody @Valid TrackingOrderDetailReqDTO trackingOrderReq) {
		try {
			TrackingOrderDetailResDTO dataDetail = trackingService.getTrackingOrderDetail(trackingOrderReq);

			return new ResponseEntity<>(dataDetail, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "getTrackingOrderTagihan", nickname = "getTrackingOrderTagihan")
	@PostMapping("/trackingordertagihan")
	public ResponseEntity<Object> getTrackingOrderTagihan(@RequestBody TrackingOrderTagihanReqDTO trackingOrderReq) {
		List<TrackingOrderTagihanResDTO> dataHome = new ArrayList<>();
		try {

			dataHome = trackingService.getListTrackingTagihan(trackingOrderReq);

			LOGGER.info("tracking tagihan success");
			return new ResponseEntity<>(dataHome, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("tracking tagihan failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "printPo", nickname = "printPO")
	@PostMapping("/printPo")
	public ResponseEntity<byte[]> printPO(@RequestBody PoEcmReqDTO poRequest) {
		byte[] dataHome = null;
		try {

			dataHome = trackingService.fileEcmPo(poRequest);

			LOGGER.info("tracking home success");
			return new ResponseEntity<>(dataHome, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("tracking home failed {}", e.getMessage());
			return new ResponseEntity<>(dataHome, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "getTrackingOrderWeb", nickname = "getTrackingOrderWeb")
	@PostMapping("/trackingorderweb")
	public ResponseEntity<Object> getTrackingOrderWeb(@RequestBody TrackingOrderWebReqDTO trackingOrderReq) {
		List<TrackingOrderHomeResDTO> dataHome = new ArrayList<>();
		try {
			if (trackingOrderReq.getUserType().equalsIgnoreCase("1"))
				dataHome = trackingService.getTrackingOrderWebSales(trackingOrderReq);
			else
				dataHome = trackingService.getTrackingOrderWebOwner(trackingOrderReq);

			LOGGER.info("tracking Order Web success");
			return new ResponseEntity<>(dataHome, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("tracking Order Web failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "getSummaryTrackingOrder", nickname = "getSummaryTrackingOrder")
	@PostMapping("/summarytrackingorder")
	public ResponseEntity<Object> getSummaryTrackingOrder(@RequestBody TrackingOrderWebReqDTO trackingOrderReq) {
		List<TrackingOrderSummaryResDTO> dataHome = new ArrayList<>();
		try {
			dataHome = trackingService.getSummaryTrackingOrder(trackingOrderReq);

			LOGGER.info("tracking Order Web success");
			return new ResponseEntity<>(dataHome, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("tracking Order Web failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "getSummaryTagihan", nickname = "getSummaryTagihan")
	@PostMapping("/summarytagihan")
	public ResponseEntity<Object> getSummaryTagihan(@RequestBody TrackingOrderTagihanReqDTO trackingOrderReq) {
		List<TrackingOrderSummaryResDTO> dataHome = new ArrayList<>();
		try {
			dataHome = trackingService.getSummaryTagihan(trackingOrderReq);

			LOGGER.info("tracking Tagihan Web success");
			return new ResponseEntity<>(dataHome, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("tracking Tagihan Web failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "getNotification", nickname = "getNotification")
	@PostMapping("/notification/{userId}")
	public ResponseEntity<Object> getNotification(@PathVariable("userId") String userId) {
		List<NotificationResDTO> notification = new ArrayList<>();
		try {
			notification = trackingService.getNotification(userId);

			LOGGER.info("Get Notification Success");
			return new ResponseEntity<>(notification, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Get Notification failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "updateNotification", nickname = "updateNotification")
	@PostMapping("/updatenotif/{notifId}")
	public ResponseEntity<Object> updateNotif(@PathVariable("notifId") String notifId) {
		var responses = StatusConst.SUCCESS.getMessage();
		try {

			trackingService.updateNotification(notifId);

			LOGGER.info("Update Notification Success");
			return new ResponseEntity<>(responses, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Get Notification failed {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "get-list-pencairan", nickname = "get-list-pencairan")
	@PostMapping("/list-pencairan")
	public ResponseEntity<Object> listPencairan(@RequestBody PembiayaanReqDTO pembiayaanRequst) {
		List<ListOrderPencairanResDTO> response = new ArrayList<>();
		try {

			response = trackingService.getListPencairan(pembiayaanRequst);

			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Get List Notification {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "get-summary-pencairan", nickname = "get-summary-pencairan")
	@PostMapping("/summary-pencairan")
	public ResponseEntity<Object> headerPencairan(@RequestBody PembiayaanReqDTO pembiayaanRequst) {
		List<SummaryHeaderPencairanResDTO> response = new ArrayList<>();
		try {

			response = trackingService.getSummaryPencairan(pembiayaanRequst);

			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Get List Notification {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@ApiOperation(value = "get-list--cabang-pencairan", nickname = "get-list--cabang-pencairan")
	@GetMapping("/list-cabang-pencairan/partnerId={partnerId}")
	public ResponseEntity<Object> listCabangPencairan(@PathVariable("partnerId") String partnerId) {
		List<CabangPencairanResDTO> response = new ArrayList<>();
		try {

			response = trackingService.getListCabangPencairan(partnerId);

			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Get List Notification {}", e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
