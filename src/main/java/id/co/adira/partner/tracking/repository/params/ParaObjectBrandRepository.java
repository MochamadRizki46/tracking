package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaObjectBrand;

@Repository
public interface ParaObjectBrandRepository extends JpaRepository<ParaObjectBrand, String> {
	
	@Query("select distinct o.paraObjectBrandName from ParaObjectBrand o where o.paraObjectBrandId = :objectId")
	String findByObjectId(@Param("objectId") String objectId);

}
