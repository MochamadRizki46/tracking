/**
 * 
 */

package id.co.adira.partner.tracking.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Vionza
 *
 */

@Table(name = "sap_notif_payment")
@Entity
@Data
public class NotifPayment {
	@Id
	@Column(name = "sap_notif_payment_id")
	String notifPaymentId;
	@Column(name = "tanggal_cair")
	Date tanggalCair;
	@Column(name = "jenis_cair")
	String jenisCair;
	@Column(name = "jenis_komisi")
	String jenisKomisi;
	@Column(name = "np_doc_no_invoice")
	String docInvoiceNo;
	@Column(name = "np_doc_no_payment")
	String docPaymentNo;
	@Column(name = "np_accountholder")
	String namaRekeningPenerima;
	@Column(name = "np_norekening")
	String noRekeningPenerima;
	@Column(name = "np_bank_code")
	String kodeBankPenerima;
	@Column(name = "np_bank")
	String namaBankPenerima;
	@Column(name = "appl_no")
	String applicationNo;
	@Column(name = "appl_contract_no")
	String contractNo;
	@Column(name = "appl_br_id")
	String branchNo;
	@Column(name = "bp_ext")
	String bpPartner;
	@Column(name = "amt_cair")
	BigDecimal jumlahPencairan;
	@Column(name = "np_bp_vendor_no")
	String vendorNo;
	@Column(name = "status_transfer")
	String statusTransfer;
	@Column(name = "job")
	String job;
	
}



