package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaKecamatan;

@Repository
public interface ParaKecamatanRepository  extends JpaRepository<ParaKecamatan, String> {

	@Query("select o from ParaKecamatan o where o.paraKecamatanId = :kecamatanId")
	ParaKecamatan findByKecamatanId(@Param("kecamatanId") String kecamatanId);

}
