package id.co.adira.partner.tracking.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.dto.responses.TrackingOrderDetailResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHomeResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingSumOrderSlsResDTO;
import id.co.adira.partner.tracking.entity.SubmitOrder;

@Repository
@Transactional
public interface SubmitOrderRepository extends JpaRepository<SubmitOrder, String> {

	@Query("select o from SubmitOrder o where o.orderNo = :orderNo")
	SubmitOrder findByOrderNo(@Param("orderNo") String orderNo);
}
