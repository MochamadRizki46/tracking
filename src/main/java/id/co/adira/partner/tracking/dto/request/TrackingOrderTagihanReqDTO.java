/**
 * 
 */
package id.co.adira.partner.tracking.dto.request;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
public class TrackingOrderTagihanReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Dealer Code must be filled!")
	String dealCode;
	@ApiModelProperty(position = 2)
	String groupStatus;
	@ApiModelProperty(position = 3)
	String startDate;
	@ApiModelProperty(position = 4)
	String endDate;
}
