/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
@AllArgsConstructor
public class TrackingOrderTagihanResDTO {
	@ApiModelProperty(position = 1)
	private String orderDate;
	
	@ApiModelProperty(position = 2)
	private String applNoUnit;
	
	@ApiModelProperty(position = 3)
	private String branchId;
	
	@ApiModelProperty(position = 4)
	private String custName;
	
	@ApiModelProperty(position = 5)
	private String objectModelDesc;
	
	@ApiModelProperty(position = 6)
	private BigDecimal totalAmount;
	
	@ApiModelProperty(position = 7)
	private String statusDate;
	
	@ApiModelProperty(position = 8)
	private String status;
	
	@ApiModelProperty(position = 9)
	private String groupStatus;
	
	@ApiModelProperty(position = 10)
	private String skPo;

}

