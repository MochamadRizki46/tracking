/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.ApplicationInsurance;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationInsuranceRepository extends JpaRepository<ApplicationInsurance, String> {

}
