package id.co.adira.partner.tracking.entity.params;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "para_fin_type")
@Data
public class ParaFinType {

	@Id
	@Column(name = "para_fin_type_id")
	private int paraFinTypeId;
	@Column(name = "para_fin_type_name")
	private String paraFinTypeName;
	@Column(name = "effective_date")
	private String effectiveDate;
	@Column(name = "active")
	private int active;
	@Column(name = "logid")
	private BigDecimal logid;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
}
