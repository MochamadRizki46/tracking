/**
 * 
 */
package id.co.adira.partner.tracking.dto.request;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Vionza
 *
 */

@Data
public class TrackingOrderWebReqDTO {

	@ApiModelProperty(position = 1)
	@NotEmpty(message = "User Id must be filled!")
	String userId;
	@ApiModelProperty(position = 2)
	String userType;
	@ApiModelProperty(position = 3)
	@NotEmpty(message = "Start Date must be filled!")
	String groupStatus;
	@ApiModelProperty(position = 4)
	@NotEmpty(message = "Start Date must be filled!")
	String startDate;
	@ApiModelProperty(position = 5)
	@NotEmpty(message = "End Date must be filled!")
	String endDate;
}
