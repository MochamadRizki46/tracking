/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.ApplicationObject;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationObjectRepository extends JpaRepository<ApplicationObject, String> {
	@Query("select o from ApplicationObject o where o.applicationNoUnit = :applUnitNo")
	ApplicationObject findByApplUnitNo(@Param("applUnitNo") String applUnitNo);
}
