package id.co.adira.partner.tracking.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "tbl_order_domisili")
@Data
public class SubmitDomisili implements Serializable {

	@Id
	@Column(name = "submit_order_id")
	String submitOrderId;

	@Column(name = "flag_address_id")
	Integer flagAddressId;
	@Column(name = "address_domisili")
	String addressDomisili;	
	@Column(name = "survey_date")
	Date surveyDate;
	@Column(name = "survey_time")
	String surveyTime;	
	@Column(name = "rtrw")
	String rtrw;
	@Column(name = "kelurahan")
	String kelurahan;	
	@Column(name = "kelurahan_desc")
	String kelurahanDesc;
	@Column(name = "kecamatan")
	String kecamatan;	
	@Column(name = "kecamatan_desc")
	String kecamatanDesc;
	@Column(name = "kabkot")
	String kabkot;	
	@Column(name = "kabkot_desc")
	String kabkotDesc;
	@Column(name = "provinsi")
	String provinsi;	
	@Column(name = "provinsi_desc")
	String provinsiDesc;
	@Column(name = "zipcode")
	String zipcode;
	@CreationTimestamp
	@Column(name = "created_date")
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@UpdateTimestamp
	@Column(name = "modified_date")
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "active")
	Integer active;
	@Column(name = "nama_so")
	String namaSo;

}