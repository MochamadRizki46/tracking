package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaFinType;

@Repository
public interface ParaFinTypeRepository extends JpaRepository<ParaFinType, String> {
	
	@Query("select o.paraFinTypeName from ParaFinType o where o.paraFinTypeId = :id")
	String findNameById(@Param("id") int id);

}
