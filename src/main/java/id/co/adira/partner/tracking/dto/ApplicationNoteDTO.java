/**
 * 
 */
package id.co.adira.partner.tracking.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
public class ApplicationNoteDTO {
	private String skApplicationNote;
    private String applicationNo;
    private boolean flag;
    private String note;
    private boolean active;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
}
