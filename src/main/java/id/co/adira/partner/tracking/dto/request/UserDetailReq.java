package id.co.adira.partner.tracking.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserDetailReq {
	@NotEmpty(message = "User Id must be filled!")
	String userId;

}
