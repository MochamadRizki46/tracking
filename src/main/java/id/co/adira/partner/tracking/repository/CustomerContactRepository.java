package id.co.adira.partner.tracking.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.CustomerContact;

@Repository
@Transactional
public interface CustomerContactRepository extends JpaRepository<CustomerContact, String> {

}
	