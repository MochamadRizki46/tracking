package id.co.adira.partner.tracking.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppConst {
    DRAFT("Draft"),
    CONTENT_TYPE("Content-Type"),
    MULTIPART_FORM_DATA("multipart/form-data"),
    X_API_KEY("x-api-key"),
    DOCUMENT_TYPE("documentType"),
    NO_NPWP("NoNPWP"),
    NPWP_CUSTOMER("NPWPCustomer"),
    KTP_CUSTOMER("KTPCustomer"),
    NO_KTP("NoKTP"),
    FILE("file"),
    DOCUMENT_TITLE("documentTitle"),
    REGION("Region"),
    APPLICATION("application"),
    A1PARTNER("A1PARTNER"),
    OBJECT_STORE("objectStore"),
    ADIRA_OS("ADIRAOS"),
    REQUEST_ID("requestId"),
    ONE_TO_FIVE("12345"),
    SALES_OFFICER("salesOfficer"),
    APPL_NO_UNIT("applNoUnit"),
    ACCTION("ACCTION"),
    ID("id.co"),
    TANGGAL_SURVEY("tanggalSurvey"),
    JAM_SURVEY("jamSurvey"),
    FILES("files"),
    GROUPSTATUS_LE_TRACK("GS01"),
    STATUS_LE_TRACK("ST01"),
	PARA_TRACKING_UPDATE_LE("system-ad1Partner"),
	CUST_PERSONAL("PER"),
	TIPE_INSURANCE_COMBI(" Tahun Pertama COMPREHENSIVE (COM), Sisa TOTAL LOSS ONLY (TLO)"),
	TIPE_INSURANCE_SINGLE_TLO("TOTAL LOSS ONLY (TLO)"),
	TIPE_INSURANCE_SINGLE_COMPRE("COMPREHENSIVE (COM)");
    


    private String message;

}
