/**
 * 
 */
package id.co.adira.partner.tracking.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PoEcmReqDTO {
	String skPo;
}
