/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderHomeResDTO {
	@ApiModelProperty(position = 1)
	private String orderNo;
	@ApiModelProperty(position = 2)
	private String applNo;
	@ApiModelProperty(position = 3)
	private String applNoUnit;
	@ApiModelProperty(position = 4)
	private String custName;
	@ApiModelProperty(position = 5)
	private String orderDate;
	@ApiModelProperty(position = 6)
	private String status;
	@ApiModelProperty(position = 7)
	private String objectDesc;
	@ApiModelProperty(position = 8)
	private String objectModelDesc;
	@ApiModelProperty(position = 9)
	private String groupStatus;
	@ApiModelProperty(position = 10)
	private String branchName;
	@ApiModelProperty(position = 11)
	private String skPo;
}
