/**
 * 
 */
package id.co.adira.partner.tracking.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;


import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
public class ApplicationDTO {
	private String skApplication;
    private String applicationNo;
    private String fkCustomer;
    private String sourceApplication;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderDate;
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applicationDate;
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date surveyAppointmentDate;
    
    private String orderType;
    private boolean flagAccount;
    private String accountNoFom;
    private String branchSurvey;
    private String branchSales;
    private String initialRecomendation;
    private String finalRecomendation;
    private int signPk;
    private String konsepType;
    private int objectQty;
    private String propInsrType;
    private int unit;
    private String applicationContractNo;
    private String applicationLastGroupStatus;
    private String applicationLastStatus;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
    private boolean active;
    private boolean flagIA;
    private String dealerNote;
    private String orderNo;
    private List<ApplicationObjectDTO> applicationObject;
    private ApplicationDocumentDTO[] applicationDocument;
    private ApplicationNoteDTO[] applicationNote;
}
