/**
 * 
 */
package id.co.adira.partner.tracking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.adira.partner.tracking.entity.UserDetail;
import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
@Entity
@Table(name = "para_user_detail")
public class UserDetail {
	@Id
	@Column(name = "id")
	int id;
	@Column(name = "userid")
	String userId;
	@Column(name = "partner_code")
	String partnerCode;
	@Column(name ="isactive")
	Integer active;
}
