/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import java.math.BigDecimal;
import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SummaryHeaderPencairanResDTO {

	String status;
	BigDecimal totalPencairan;
}
