/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CabangPencairanResDTO {
	String branchNo;
	String branchName;
}
