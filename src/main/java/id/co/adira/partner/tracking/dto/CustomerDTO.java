/**
 * 
 */
package id.co.adira.partner.tracking.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
public class CustomerDTO {

	private String skCustomer;
	private String customerId;
	private String customerOid;
	private String customerType;
	private String npwpAddress;
	private String npwpName;
	private String npwpNo;
	private String npwpType;
	private boolean flagNpwp;
	private String flagPkpSign;
	private boolean flagGuarantor;
	private boolean active;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private CustomerPersonalDTO customerPersonal;
	private CustomerCompanyDTO customerCompany;
	private List<CustomerAddressDTO> customerAddress;
}
