package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaObject;

@Repository
public interface ParaObjectRepository  extends JpaRepository<ParaObject, String> {

	@Query("select o from ParaObject o where o.paraObjectId = :objectId")
	ParaObject findByObjectId(@Param("objectId") String objectId);

}
