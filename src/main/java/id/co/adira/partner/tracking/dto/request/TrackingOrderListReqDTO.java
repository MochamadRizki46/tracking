/**
 * 
 */
package id.co.adira.partner.tracking.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * @author Vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderListReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "User Id must be filled!")
	String userId;
	@ApiModelProperty(position = 2)
	String userType;
	@ApiModelProperty(position = 3)
	@NotEmpty(message = "flag must be filled!")
	int flag;
	@ApiModelProperty(position = 4)
	@NotEmpty()
	int sort;
}
