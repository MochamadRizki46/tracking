package id.co.adira.partner.tracking.repository.params;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.entity.params.ParaObjectModel;

@Repository
public interface ParaObjectModelRepository extends JpaRepository<ParaObjectModel, String> {
	
	@Query("select distinct o.paraObjectModelName from ParaObjectModel o where o.paraObjectModelId = :objectId")
	String findByObjectId(@Param("objectId") String objectId);

}
