/**
 * 
 */
package id.co.adira.partner.tracking.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Table(name = "tbl_notification")
@AllArgsConstructor
@Data
public class Notification {

	@Id
	@Column(name = "notif_id")
	private String notifId;
	@Column(name = "user_id")
	private String userId;
	@Column(name = "order_no")
	private String orderNo;
	@Column(name = "application_no")
	private String applicationNo;
	@Column(name = "application_no_unit")
	private String applicationNoUnit;
	@Column(name = "title")
	private String title;
	@Column(name = "body")
	private String body;
	@Column(name = "status")
	private String status;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "isread")
	private boolean isRead;
	@Column(name = "active")
	private boolean active;

}
