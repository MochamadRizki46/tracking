/**
 * 
 */
package id.co.adira.partner.tracking.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PembiayaanReqDTO {

	@ApiModelProperty(position = 1)
	String partnerType;
	@ApiModelProperty(position = 2)
	String partnerId;
	@ApiModelProperty(position = 3)
	String productType;
	@ApiModelProperty(position = 4)
	String startDate;
	@ApiModelProperty(position = 5)
	String endDate;
	@ApiModelProperty(position = 6)
	String branchNo;
}
