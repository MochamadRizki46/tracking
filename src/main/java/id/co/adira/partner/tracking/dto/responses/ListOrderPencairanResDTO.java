/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;


import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ListOrderPencairanResDTO {
	
	@ApiModelProperty(position = 1)
	private String tanggalCair;
	
	@ApiModelProperty(position = 2)
	private String jenisCair;
	
	@ApiModelProperty(position = 3)
	private String docInvoiceNo;
	
	@ApiModelProperty(position = 4)
	private String namaRekeningPenerima;
	
	@ApiModelProperty(position = 5)
	private String noRekeningPenerima;
	
	@ApiModelProperty(position = 6)
	private String custName;
	
	@ApiModelProperty(position = 7)
	private String objectDesc;
	
	@ApiModelProperty(position = 8)
	private String branchName;
	
	@ApiModelProperty(position = 9)
	private String applicationNo;
	
	@ApiModelProperty(position = 10)
	private BigDecimal jumlahPencairan;
	
	@ApiModelProperty(position = 11)
	private BigDecimal jumlahKomisi;
	
	@JsonIgnore
	private String branchId;

}
