package id.co.adira.partner.tracking.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.*;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;


import lombok.Getter;
import lombok.Setter;

/**
 * @author 10999943
 *
 */
@Entity
@Table(name = "tbl_doc_dtl")
@Setter
@Getter
@TypeDefs({ @TypeDef(name = "json", typeClass = JsonStringType.class),
		@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
@Embeddable
public class SubmitDocument implements Serializable {

	
	@Id
	@Column(name = "doc_detail_id")
	String docDetailId;
	@Column(name = "id")
	String id;
	@Column(name = "doc_id")
	String docId;
	@Column(name = "doc_path")
	String docPath;
	@Column(name = "active")
	Integer active;
	@Column(name = "created_date")
	@CreationTimestamp
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "modified_date")
	@UpdateTimestamp
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "doc_type")
	String docType;
	@Column(name = "submit_order_id")
	String submitOrderId;
}
