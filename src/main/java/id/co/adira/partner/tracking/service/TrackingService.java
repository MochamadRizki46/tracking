package id.co.adira.partner.tracking.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.tracking.constant.AppConst;
import id.co.adira.partner.tracking.constant.GroupStatusTracking;
import id.co.adira.partner.tracking.dto.request.PembiayaanReqDTO;
import id.co.adira.partner.tracking.dto.request.PoEcmReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderDetailReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderHeaderReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderHistoryReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderListReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderTagihanReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderWebReqDTO;
import id.co.adira.partner.tracking.dto.responses.SummaryHeaderPencairanResDTO;
import id.co.adira.partner.tracking.dto.responses.CabangPencairanResDTO;
import id.co.adira.partner.tracking.dto.responses.ListOrderPencairanResDTO;
import id.co.adira.partner.tracking.dto.responses.NotificationResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderDetailResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHeaderResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHistoryResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHomeResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderSummaryResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderTagihanResDTO;
import id.co.adira.partner.tracking.entity.Application;
import id.co.adira.partner.tracking.entity.ApplicationObject;
import id.co.adira.partner.tracking.entity.ApplicationSalesInfo;
import id.co.adira.partner.tracking.entity.Customer;

import id.co.adira.partner.tracking.entity.CustomerAddress;
import id.co.adira.partner.tracking.entity.CustomerContact;
import id.co.adira.partner.tracking.entity.SubmitOrder;
import id.co.adira.partner.tracking.entity.params.ParaKecamatan;
import id.co.adira.partner.tracking.entity.params.ParaKelurahan;
import id.co.adira.partner.tracking.externalapi.TrackingExternalApi;
import id.co.adira.partner.tracking.repository.*;
import id.co.adira.partner.tracking.repository.params.ParaFinTypeRepository;
import id.co.adira.partner.tracking.repository.params.ParaKecamatanRepository;
import id.co.adira.partner.tracking.repository.params.ParaKelurahanRepository;
import id.co.adira.partner.tracking.repository.params.ParaObjectBrandRepository;
import id.co.adira.partner.tracking.repository.params.ParaObjectModelRepository;
import id.co.adira.partner.tracking.repository.params.ParaObjectRepository;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import static id.co.adira.partner.tracking.constant.AppConst.*;
import static id.co.adira.partner.tracking.constant.FormattingConst.*;
import static id.co.adira.partner.tracking.constant.StatusConst.*;

@Service
public class TrackingService {

	@Autowired
	DistributedApplicationTrackingRepository applicationTrackingRepository;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	SubmitOrderRepository submitOrderRepository;

	@Autowired
	ParaKelurahanRepository paraKelurahanRepository;

	@Autowired
	ParaKecamatanRepository paraKecamatanRepository;

	@Autowired
	ParaObjectRepository paraObjectRepository;

	@Autowired
	ParaFinTypeRepository paraFinTypeRepository;

	@Autowired
	ParaObjectBrandRepository paraObjectBrandRepository;

	@Autowired
	ParaObjectModelRepository paraObjectModelRepository;

	@Autowired
	PurchaseOrderRepository poRepository;

	@Autowired
	NotificationRepository notifrepository;

	@Autowired
	NotifPaymentRepository notifPaymentRepository;

	@Autowired
	ObjectMapper objectMapper;

	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	TrackingExternalApi externalApi;

	private static final Logger LOGGER = LoggerFactory.getLogger(TrackingService.class);

	String data = "";
	String customerName = data;
	String objectCode = data;
	String objectDesc = data;
	String objectModel = data;
	String objectModelDesc = data;
	String salesName = data;
	String surveyDate = data;
	String surveyTime = data;
	String nikSales = data;
	String applicationNoUnit = data;

	SimpleDateFormat sdf = new SimpleDateFormat(FIRST_SIMPLE_DATE_FORMAT.getMessage());
	SimpleDateFormat formatTime = new SimpleDateFormat(TIME_FORMAT.getMessage());
	SimpleDateFormat secondSdf = new SimpleDateFormat(SECOND_SIMPLE_DATE_FORMAT.getMessage());
	DateTimeFormatter formatDate = DateTimeFormatter.ofPattern(FIRST_SIMPLE_DATE_FORMAT.getMessage());

	public TrackingOrderHeaderResDTO getTrackingOrderHeader(TrackingOrderHeaderReqDTO request) {
		var dataHeaderResponse = new TrackingOrderHeaderResDTO();
		System.out.println("masuk1");
		var application = applicationRepository.findByNoApplication(request.getApplPayung());
		if (application != null) {
			try {
				dataHeaderResponse = mapTrackingHeaderApplication(request);

			} catch (Exception e) {
				LOGGER.error(FAILED_TO_GET_TRACKING_HEADER_APPL.getMessage(), e.getMessage());
			}
		} else {
			try {
				dataHeaderResponse = mapTrackingHeaderPartner(request);

			} catch (Exception e) {
				LOGGER.error(FAILED_TO_GET_TRACKING_HEADER_PARTNER.getMessage(), e.getMessage());
			}
		}

		return dataHeaderResponse;
	}

	public TrackingOrderHeaderResDTO mapTrackingHeaderApplication(TrackingOrderHeaderReqDTO request) {
		TrackingOrderHeaderResDTO dataHeader = new TrackingOrderHeaderResDTO();

		var application = applicationRepository.findByNoApplication(request.getApplPayung());
		var customer = customerRepository.findByCustomerId(application.getFkCustomer());
		var orderFromPartner = submitOrderRepository.findByOrderNo(application.getOrderNo());

		// di remark dikarnakan kebutuhan chat dan belum mulai untuk multiunit
		// var orderFromPartner =
		// submitOrderRepository.findByOrderNo(request.getOrderNo());

//		Optional<ApplicationObject> findUnit = application.getApplicationObject().stream()
//				.filter(orderUnit -> orderUnit.getApplicationNoUnit().equalsIgnoreCase(request.getApplNoUnit()))
//				.findAny();

		if (customer != null) {
			LOGGER.info("customer");
			customerName = customer.getCustomerType().equalsIgnoreCase(CUST_PERSONAL.getMessage())
					? customer.getCustomerPersonal().getIdentityFullName()
					: customer.getCustomerCompany().getCompanyName();
		} else {
			customerName = orderFromPartner.getSubmitCustomer().getCustomerName();
		}

		// if (findUnit.isPresent()) {
		if (!application.getApplicationObject().isEmpty()) {
			LOGGER.info("unit");

			// ApplicationObject applicationUnit = findUnit.get();
			ApplicationObject applicationUnit = application.getApplicationObject().get(0);

			Optional<ApplicationSalesInfo> findSales = applicationUnit.getApplicationSalesInfo().stream()
					.filter(e -> e.getSalesType().equalsIgnoreCase("001") && e.isActive()).findFirst();

			var objectSales = new ApplicationSalesInfo();

			if (findSales.isPresent()) {
				objectSales = findSales.get();

			}
			LOGGER.info(applicationUnit.getObject());

			applicationNoUnit = applicationUnit.getApplicationNoUnit();
			objectCode = applicationUnit.getObject();
			objectModel = applicationUnit.getObjectModel();
			salesName = objectSales.getEmployeeName();
			nikSales = objectSales.getEmployeeId();

			var paraObject = paraObjectRepository.findByObjectId(objectCode);
			var paraObjectModel = paraObjectModelRepository.findByObjectId(objectModel);

			objectDesc = paraObject.getParaObjectName();
			objectModelDesc = paraObjectModel;

		} else {
			objectCode = orderFromPartner.getSubmitUnit().getObject();
			objectModel = orderFromPartner.getSubmitUnit().getObjectModel();
			objectDesc = orderFromPartner.getSubmitUnit().getObjectDesc();
			objectModelDesc = orderFromPartner.getSubmitUnit().getObjectModelDesc();
		}

		surveyDate = application.getSurveyAppointmentDate() != null
				? secondSdf.format(application.getSurveyAppointmentDate())
				: secondSdf.format(orderFromPartner.getSubmitDomisili().getSurveyDate());
		surveyTime = application.getSurveyAppointmentDate() != null
				? formatTime.format(application.getSurveyAppointmentDate())
				: orderFromPartner.getSubmitDomisili().getSurveyTime();

		dataHeader.setOrderNo(application.getOrderNo());
		dataHeader.setApplNoUnit(applicationNoUnit);
		dataHeader.setCustName(customerName);
		dataHeader.setOrderDate(sdf.format(application.getOrderDate()));
		dataHeader.setStatus(data);
		dataHeader.setObject(objectCode);
		dataHeader.setObjectDesc(objectDesc);
		dataHeader.setObjectModel(objectModel);
		dataHeader.setObjectModelDesc(objectModelDesc);
		dataHeader.setNamaSo(salesName);
		dataHeader.setSurveyDate(surveyDate);
		dataHeader.setSurveyTime(surveyTime);
		dataHeader.setNikSo(nikSales);

		return dataHeader;
	}

	public TrackingOrderHeaderResDTO mapTrackingHeaderPartner(TrackingOrderHeaderReqDTO request) {
		TrackingOrderHeaderResDTO dataHeader = new TrackingOrderHeaderResDTO();

		var orderFromPartner = submitOrderRepository.findByOrderNo(request.getOrderNo());

		dataHeader.setOrderNo(orderFromPartner.getOrderNo());
		dataHeader.setApplNoUnit(orderFromPartner.getApplNoUnit());
		dataHeader.setCustName(orderFromPartner.getSubmitCustomer().getCustomerName());
		dataHeader.setOrderDate(sdf.format(orderFromPartner.getOrderDate()));
		dataHeader.setStatus(data);
		dataHeader.setObject(orderFromPartner.getSubmitUnit().getObject());
		dataHeader.setObjectDesc(orderFromPartner.getSubmitUnit().getObjectDesc());
		dataHeader.setObjectModel(orderFromPartner.getSubmitUnit().getObjectModel());
		dataHeader.setObjectModelDesc(orderFromPartner.getSubmitUnit().getObjectModelDesc());
		dataHeader.setNamaSo(orderFromPartner.getSubmitDomisili().getNamaSo());
		dataHeader.setSurveyDate(secondSdf.format(orderFromPartner.getSubmitDomisili().getSurveyDate()));
		dataHeader.setSurveyTime(orderFromPartner.getSubmitDomisili().getSurveyTime());
		dataHeader.setNikSo(data);

		return dataHeader;
	}

	public List<TrackingOrderHistoryResDTO> getTrackingOrderHistoryOwn(TrackingOrderHistoryReqDTO getTrackingHistReq) {

		List<TrackingOrderHistoryResDTO> response = new ArrayList<>();
		var param = getTrackingHistReq.getApplNo();
		var msgErrorTrackHist = "Error Get Tracking Order History - {}";

		if (getTrackingHistReq.getApplNo().substring(0, 3).equalsIgnoreCase("A1P")) {
			try {
				System.out.println(param);
				response = applicationTrackingRepository.findByOrderNo(param);
				System.out.println(response);
				response.stream().forEach(e -> {
					if (e.getKodeGroupStatus() != null)
						e.setKodeGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getKodeGroupStatus()));
				});

			} catch (Exception e) {
				LOGGER.error(msgErrorTrackHist, e.toString());
			}
		} else {
			try {
				param = getTrackingHistReq.getApplNo().substring(0, 11);
				response = applicationTrackingRepository.findByApplUnitNo(param);
				response.stream().forEach(e -> {
					if (e.getKodeGroupStatus() != null)
						e.setKodeGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getKodeGroupStatus()));
				});

			} catch (Exception e) {
				LOGGER.error(msgErrorTrackHist, e.toString());
			}
		}

		return response;
	}

	public List<TrackingOrderHistoryResDTO> getTrackingOrderHistorySls(TrackingOrderHistoryReqDTO getTrackingHistReq) {

		List<TrackingOrderHistoryResDTO> response = new ArrayList<>();

		var param = getTrackingHistReq.getApplNo();
		var msgErrorTrackHist = "Error Get Tracking Order History - {}";

		if (getTrackingHistReq.getApplNo().substring(0, 3).equalsIgnoreCase("A1P")) {
			try {
				response = applicationTrackingRepository.findByOrderNo(param);
				response.stream().forEach(e -> {
					if (e.getKodeGroupStatus() != null)
						e.setKodeGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getKodeGroupStatus()));
				});

			} catch (Exception e) {
				LOGGER.error(msgErrorTrackHist, e.toString());
			}
		} else {
			try {
				param = getTrackingHistReq.getApplNo().substring(0, 11);
				response = applicationTrackingRepository.findByApplUnitNo(param);
				response.stream().forEach(e -> {
					if (e.getKodeGroupStatus() != null)
						e.setKodeGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getKodeGroupStatus()));
				});

			} catch (Exception e) {
				LOGGER.error(msgErrorTrackHist, e.toString());
			}
		}

		return response;
	}

	public List<TrackingOrderHomeResDTO> getListTrackingHomeOwn(TrackingOrderListReqDTO trackingOrderReq) {

		List<TrackingOrderHomeResDTO> response = new ArrayList<>();

		try {
			List<TrackingOrderHomeResDTO> tracking = applicationTrackingRepository
					.findByDateAndDlc(getFilterDate(trackingOrderReq.getFlag()), trackingOrderReq.getUserId());

			tracking.stream().forEach(e -> {
				if (e.getGroupStatus() != null)
					e.setGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getGroupStatus()));
			});
			response.addAll(tracking);
			if (trackingOrderReq.getSort() != 0)
				Collections.reverse(response);
		} catch (Exception e) {
			LOGGER.error("Response Error TradkingOrderHomeOwn: {}", e.toString());
		}

		return response;
	}

	public List<TrackingOrderHomeResDTO> getListTrackingHomeSls(TrackingOrderListReqDTO trackingOrderReq) {

		List<TrackingOrderHomeResDTO> response = new ArrayList<>();

		try {
			List<TrackingOrderHomeResDTO> tracking = applicationTrackingRepository
					.findByDateAndUserid(getFilterDate(trackingOrderReq.getFlag()), trackingOrderReq.getUserId());

			tracking.stream().forEach(e -> {
				if (e.getGroupStatus() != null && e.getStatus() != null)
					e.setGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getGroupStatus()));
			});

			response.addAll(tracking);
			if (trackingOrderReq.getSort() != 0)
				Collections.reverse(response);
		} catch (Exception e) {
			LOGGER.error("Response Error TradkingOrderHomeSales: {}", e.toString());
		}

		return response;
	}

	public List<TrackingOrderHomeResDTO> getTrackingOrderWebOwner(TrackingOrderWebReqDTO trackingOrderReq) {

		List<TrackingOrderHomeResDTO> response = new ArrayList<>();
		List<Tuple> tracking = new ArrayList<>();

		try {
			if (trackingOrderReq.getGroupStatus().toUpperCase().contains(GroupStatusTracking.BERLANGSUNG.name())) {
				tracking = applicationTrackingRepository.findByRangeDateAndDlcFillterByOrderBerlangsung(
						trackingOrderReq.getStartDate(), trackingOrderReq.getEndDate(), trackingOrderReq.getUserId(),
						trackingOrderReq.getUserType());
			} else {
				if (trackingOrderReq.getGroupStatus().toUpperCase().contains(GroupStatusTracking.DRAFT.name())) {
					tracking = applicationTrackingRepository.findDetailOrderDraftByDateAndDlc(
							trackingOrderReq.getStartDate(), trackingOrderReq.getEndDate(),
							trackingOrderReq.getUserId());
				} else {
					tracking = applicationTrackingRepository.findByRangeDateAndDlc(trackingOrderReq.getStartDate(),
							trackingOrderReq.getEndDate(), trackingOrderReq.getUserId(),
							trackingOrderReq.getGroupStatus(), trackingOrderReq.getUserType());

//					tracking.stream().forEach(e -> {
//						if (e.getGroupStatus() != null)
//							e.setGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getGroupStatus()));
//					});
				}
			}
			response = tracking.stream().map(e -> new TrackingOrderHomeResDTO(e.get("orderNo", String.class),
					e.get("applNo", String.class), e.get("applNoUnit", String.class), e.get("custName", String.class),
					e.get("orderDate", String.class), e.get("status", String.class), e.get("objectDesc", String.class),
					e.get("objectModelDesc", String.class), e.get("groupStatus", String.class),
					e.get("branchName", String.class), e.get("skPo", String.class))).collect(Collectors.toList());

		} catch (Exception e) {
			LOGGER.error("Response Error TradkingOrderHomeOwn: {}", e.toString());
		}

		return response;
	}

	public List<TrackingOrderHomeResDTO> getTrackingOrderWebSales(TrackingOrderWebReqDTO trackingOrderReq) {

		List<TrackingOrderHomeResDTO> response = new ArrayList<>();
		List<Tuple> tracking = new ArrayList<>();

		try {
			if (trackingOrderReq.getGroupStatus().toUpperCase().contains(GroupStatusTracking.BERLANGSUNG.name())) {
				tracking = applicationTrackingRepository.findByRangeDateAndUserIdFillterByOrderBerlangsung(
						trackingOrderReq.getStartDate(), trackingOrderReq.getEndDate(), trackingOrderReq.getUserId());
			} else {
				if (trackingOrderReq.getGroupStatus().toUpperCase().contains(GroupStatusTracking.DRAFT.name())) {
					tracking = applicationTrackingRepository.findDetailOrderDraftByDateAndUserId(
							trackingOrderReq.getStartDate(), trackingOrderReq.getEndDate(),
							trackingOrderReq.getUserId());
				} else {
					tracking = applicationTrackingRepository.findByRangeDateAndUserId(trackingOrderReq.getStartDate(),
							trackingOrderReq.getEndDate(), trackingOrderReq.getUserId(),
							trackingOrderReq.getGroupStatus());

//					tracking.stream().forEach(e -> {
//						if (e.getGroupStatus() != null && e.getStatus() != null)
//							e.setGroupStatus(GroupStatusTracking.MAP.mapGroupsStatus(e.getGroupStatus()));
//					});

				}
			}

			response = tracking.stream().map(e -> new TrackingOrderHomeResDTO(e.get("orderNo", String.class),
					e.get("applNo", String.class), e.get("applNoUnit", String.class), e.get("custName", String.class),
					e.get("orderDate", String.class), e.get("status", String.class), e.get("objectDesc", String.class),
					e.get("objectModelDesc", String.class), e.get("groupStatus", String.class),
					e.get("branchName", String.class), e.get("skPo", String.class))).collect(Collectors.toList());

		} catch (Exception e) {
			LOGGER.error("Response Error: {}", e.toString());
		}

		return response;
	}

	public TrackingOrderDetailResDTO getTrackingOrderDetail(TrackingOrderDetailReqDTO trackingOrderReq) {
		var orderDetail = new TrackingOrderDetailResDTO();

		var applicationEntity = applicationRepository.findByNoApplication(trackingOrderReq.getApplNo());
		var orderFromPartner = submitOrderRepository.findByOrderNo(trackingOrderReq.getOrderNo());

		if (applicationEntity != null) {
			try {
				var customerEntity = customerRepository.findByCustomerId(applicationEntity.getFkCustomer());

				orderDetail = mapObjectDetail(applicationEntity, customerEntity, orderFromPartner,
						trackingOrderReq.getApplNoUnit());

			} catch (Exception e) {
				LOGGER.error(ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL.getMessage(), e.getMessage());
			}
		} else {
			try {

				orderDetail = mapTrackingDetailPartner(orderFromPartner);

			} catch (Exception e) {
				LOGGER.error(ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL.getMessage(), e.getMessage());
			}
		}

		return orderDetail;
	}

	TrackingOrderDetailResDTO mapTrackingDetailPartner(SubmitOrder orderPartner) {
		var orderDetailPartner = new TrackingOrderDetailResDTO();
		var objectDocumentId = data;
		var objectDocumentStnk = data;

		try {

			orderDetailPartner.setOrderNo(orderPartner.getOrderNo());
			orderDetailPartner.setIdNo(orderPartner.getSubmitCustomer().getIdNo());
			orderDetailPartner.setCustomerName(orderPartner.getSubmitCustomer().getCustomerName());
			orderDetailPartner.setHandphoneNo(orderPartner.getSubmitCustomer().getHandphoneNo());
			orderDetailPartner.setWaNo(orderPartner.getSubmitCustomer().getWaNo());
			orderDetailPartner.setAddressDomisili(orderPartner.getSubmitDomisili().getAddressDomisili());
			orderDetailPartner.setAddressDetail(orderPartner.getSubmitDomisili().getAddressDomisili().concat(", ")
					.concat(orderPartner.getSubmitDomisili().getKelurahanDesc().concat(", ")
							.concat(orderPartner.getSubmitDomisili().getKecamatanDesc().concat(", Kode Pos ")
									.concat(orderPartner.getSubmitDomisili().getZipcode()))));
			orderDetailPartner.setKelurahan(orderPartner.getSubmitDomisili().getKelurahanDesc().concat(", ")
					.concat(orderPartner.getSubmitDomisili().getKecamatanDesc().concat(", Kode Pos ")
							.concat(orderPartner.getSubmitDomisili().getZipcode())));
			orderDetailPartner.setSurveyDateTime(secondSdf.format(orderPartner.getSubmitDomisili().getSurveyDate())
					.concat(" ").concat(orderPartner.getSubmitDomisili().getSurveyTime()));
			orderDetailPartner.setObjectDetail(orderPartner.getSubmitUnit().getObjectDesc().concat(", ")
					.concat(orderPartner.getSubmitUnit().getObjectBrandDesc().concat(", ")
							.concat(orderPartner.getSubmitUnit().getObjectModelDesc())));
			orderDetailPartner.setObjectPrice(orderPartner.getSubmitUnit().getObjectPrice());
			orderDetailPartner.setDp(orderPartner.getSubmitUnit().getDp());
			orderDetailPartner.setTenor(orderPartner.getSubmitUnit().getTenor());
			orderDetailPartner.setInstallmentAmt(orderPartner.getSubmitUnit().getInstallmentAmt());
			orderDetailPartner.setPromo(orderPartner.getSubmitUnit().getPromo());
			orderDetailPartner.setObjectCode(orderPartner.getSubmitUnit().getObject());
			orderDetailPartner.setRate(orderPartner.getSubmitUnit().getRate());
			orderDetailPartner.setObjectYear(orderPartner.getSubmitUnit().getObjectYear());
			orderDetailPartner.setTipeJaminan(orderPartner.getSubmitUnit().getTipeJaminan());
			orderDetailPartner.setJenisAsuransi(getJenisAsuransiPartner(orderPartner.getSubmitUnit().getJenisAsuransi(),
					String.valueOf(orderPartner.getSubmitUnit().getInsrCompreCoverage())));
			orderDetailPartner.setMerk(orderPartner.getSubmitUnit().getObjectBrandDesc());
			orderDetailPartner.setTipePembiayaan(
					orderPartner.getSubmitCustomer().getFinType().toString().equalsIgnoreCase("2") ? "SYARIAH"
							: "KONVENSIONAL");
			orderDetailPartner.setTipeCustomer(orderPartner.getSubmitCustomer().getCustType());
			orderDetailPartner.setPicCompany(orderPartner.getSubmitCustomer().getPicCompany());

			if (orderPartner.getSubmitCustomer().getCustType().equalsIgnoreCase(CUST_PERSONAL.getMessage())) {

				List<String> listDocument = orderPartner.getSubmitDoc().stream()
						.filter(e -> e.getDocType().equalsIgnoreCase("1")).map(value -> value.getDocId())
						.collect(Collectors.toList());

				objectDocumentId = !listDocument.isEmpty() ? listDocument.get(0) : data;
			} else {
				List<String> listDocument = orderPartner.getSubmitDoc().stream()
						.filter(e -> e.getDocType().equalsIgnoreCase("2")).map(value -> value.getDocId())
						.collect(Collectors.toList());

				objectDocumentId = !listDocument.isEmpty() ? listDocument.get(0) : data;
			}

			List<String> listDocument = orderPartner.getSubmitDoc().stream()
					.filter(e -> e.getDocType().equalsIgnoreCase("3")).map(value -> value.getDocId())
					.collect(Collectors.toList());

			objectDocumentStnk = !listDocument.isEmpty() ? listDocument.get(0) : data;

			if (!objectDocumentId.isEmpty()) {
				byte[] documentByte = externalApi.getDocumentEcm(objectDocumentId, "01");
				objectDocumentId = Base64.getEncoder().encodeToString(documentByte);
			}

			// Hit Service ECM For Document BPKB
			if (!objectDocumentStnk.isEmpty()) {
				byte[] documentByte = externalApi.getDocumentEcm(objectDocumentStnk, "01");
				objectDocumentStnk = Base64.getEncoder().encodeToString(documentByte);
			}

			orderDetailPartner.setFotoCustomer(objectDocumentId);
			orderDetailPartner.setFotoSTNK(objectDocumentStnk);

		} catch (Exception e) {
			LOGGER.error(ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL.getMessage(), e.getMessage());
		}

		return orderDetailPartner;
	}

	public String getJenisAsuransiPartner(String jenisAsuransi, String tenorAsuransi1) {

		if (jenisAsuransi.equalsIgnoreCase("1")) {
			return AppConst.TIPE_INSURANCE_SINGLE_TLO.getMessage();
		}
		if (jenisAsuransi.equalsIgnoreCase("2")) {
			return AppConst.TIPE_INSURANCE_SINGLE_COMPRE.getMessage();
		}
		if (jenisAsuransi.equalsIgnoreCase("3") && !tenorAsuransi1.equalsIgnoreCase("null")) {
			return String.valueOf(Integer.valueOf(tenorAsuransi1) / 12)
					.concat(AppConst.TIPE_INSURANCE_COMBI.getMessage());
		}
		if (jenisAsuransi.equalsIgnoreCase("3")) {
			return "KOMBINASI";
		} else {
			return data;
		}

	}

	TrackingOrderDetailResDTO mapObjectDetail(Application application, Customer customer, SubmitOrder orderPartner,
			String applNoUnit) {

		var orderDetail = new TrackingOrderDetailResDTO();
		var paraKelurahan = new ParaKelurahan();
		var paraKecamatan = new ParaKecamatan();
		var custIdNo = data;
		var customerName = data;
		var handphoneNo = data;
		var waNo = data;
		var domisili = data;
		var addressDetail = data;
		var kelurahan = data;
		var surveyTime = data;
		var objectPrice = BigDecimal.ZERO;
		var dp = BigDecimal.ZERO;
		var tenor = 0;
		var installmentAmount = BigDecimal.ZERO;
		var objectCode = data;
		var rate = BigDecimal.ZERO;
		var tipeJaminan = data;
		var merk = data;
		var model = data;
		var objectDetail = data;
		var jenisAsuransi = data;
		var promo = data;
		var tahunPembuatan = data;
		var custType = data;
		var objectDocumentId = data;
		var objectDocumentBpkb = data;
		var tipeCustomer = data;
		var tipePembiayaan = data;
		var picCompany = data;
		var jaminanDanaTunai = "BPKB ";
		var kotaDomisili = data;

		try {
			if (customer != null) {
				custType = customer.getCustomerType();
				custIdNo = customer.getCustomerType().equalsIgnoreCase(CUST_PERSONAL.getMessage())
						? customer.getCustomerPersonal().getIdentityNo()
						: customer.getNpwpNo();
				customerName = customer.getCustomerType().equalsIgnoreCase(CUST_PERSONAL.getMessage())
						? customer.getCustomerPersonal().getIdentityFullName()
						: customer.getCustomerCompany().getCompanyName();
				LOGGER.info(customer.getCustomerType());

				Optional<CustomerAddress> addressDomisiliOptional = customer.getCustomerType()
						.equalsIgnoreCase(CUST_PERSONAL.getMessage())
								? customer.getCustomerAddress().stream()
										.filter(e -> e.getMatrixAddress().equalsIgnoreCase("5")
												&& e.getAddressType().equalsIgnoreCase("01") && e.isActive())
										.findFirst()
								: customer.getCustomerAddress().stream()
										.filter(e -> e.getMatrixAddress().equalsIgnoreCase("10")
												&& e.getAddressType().equalsIgnoreCase("02") && e.isActive())
										.findFirst();

				tipeCustomer = customer.getCustomerType();

				if (!tipeCustomer.equalsIgnoreCase(CUST_PERSONAL.getMessage())
						&& customer.getCustomerCompany().getManagementPic() != null) {
					picCompany = customer.getCustomerCompany().getManagementPic().getFullName();
				}
				if (!tipeCustomer.equalsIgnoreCase(CUST_PERSONAL.getMessage())) {
					picCompany = orderPartner.getSubmitCustomer().getPicCompany();
				}

//				picCompany = customer.getCustomerType().equalsIgnoreCase(CUST_PERSONAL.getMessage()) ? null
//						: orderPartner.getSubmitCustomer().getPicCompany();

				var addressDomisili = new CustomerAddress();

				if (addressDomisiliOptional.isPresent()) {

					addressDomisili = addressDomisiliOptional.get();
					domisili = addressDomisili.getAddress();
					addressDetail = addressDomisili.getAddress().concat(", ").concat(addressDomisili.getKelurahan())
							.concat(", ").concat(addressDomisili.getKecamatan()).concat(", ")
							.concat(addressDomisili.getZipCode());
					paraKelurahan = paraKelurahanRepository.findByKelurahanId(addressDomisili.getKelurahan());
					paraKecamatan = paraKecamatanRepository.findByKecamatanId(addressDomisili.getKecamatan());
					kelurahan = "kel." + paraKelurahan.getParaKelurahanName().concat(", kec.")
							.concat(paraKecamatan.getParaKecamatanName()).concat(", kodepos ")
							.concat(addressDomisili.getZipCode());

				} else {

					domisili = orderPartner.getSubmitDomisili().getAddressDomisili();
					addressDetail = orderPartner.getSubmitDomisili().getAddressDomisili().concat(", ")
							.concat(orderPartner.getSubmitDomisili().getKelurahanDesc()).concat(", ")
							.concat(orderPartner.getSubmitDomisili().getKecamatanDesc()).concat(", ")
							.concat(orderPartner.getSubmitDomisili().getZipcode());
					kelurahan = orderPartner.getSubmitDomisili().getKelurahanDesc().concat(", ")
							.concat(orderPartner.getSubmitDomisili().getKecamatanDesc()).concat(", ")
							.concat(orderPartner.getSubmitDomisili().getZipcode());

				}

				Optional<CustomerContact> custPhoneNo = customer.getCustomerContact().stream()
						.filter(e -> e.getMatrixContact().equalsIgnoreCase("5")
								&& e.getContactType().equalsIgnoreCase("2") && e.isActive() && e.getPriority().equalsIgnoreCase("UTAMA"))
						.findFirst();

				Optional<CustomerContact> custWaNo = customer.getCustomerContact().stream()
						.filter(e -> e.getMatrixContact().equalsIgnoreCase("5")
								&& e.getContactType().equalsIgnoreCase("3") && e.isActive() && e.getPriority().equalsIgnoreCase("UTAMA"))
						.findFirst();

				surveyTime = secondSdf.format(application.getSurveyAppointmentDate()).concat(" ")
						.concat(formatTime.format(application.getSurveyAppointmentDate()));

				if (custPhoneNo.isPresent()) {

					handphoneNo = custPhoneNo.get().getPhoneNo();

					if (custPhoneNo.get().isFlagHpIsWhatsapp()) {
						waNo = custPhoneNo.get().getPhoneNo();
					} else if (custWaNo.isPresent()) {
						waNo = custWaNo.get().getPhoneNo();
					} else if (orderPartner != null) {
						waNo = orderPartner.getSubmitCustomer().getWaNo();
					}

				} else if (orderPartner != null) {
					handphoneNo = orderPartner.getSubmitCustomer().getHandphoneNo();
					waNo = orderPartner.getSubmitCustomer().getWaNo();
				}

				if (handphoneNo != null && !handphoneNo.equalsIgnoreCase("")
						&& handphoneNo.substring(0, 3).equalsIgnoreCase("+62")) {
					handphoneNo = handphoneNo.replace("+62", "0");
				}

				if (waNo != null && !waNo.equalsIgnoreCase("") && waNo.substring(0, 3).equalsIgnoreCase("+62")) {
					waNo = waNo.replace("+62", "0");
				}

				if (orderPartner != null && orderPartner.getSubmitUnit().getKotaDomisili() != null) {

					kotaDomisili = orderPartner.getSubmitUnit().getKotaDomisili();

				}

			} else {
				custType = orderPartner.getSubmitCustomer().getCustType();
				custIdNo = orderPartner.getSubmitCustomer().getIdNo();
				customerName = orderPartner.getSubmitCustomer().getCustomerName();
				handphoneNo = orderPartner.getSubmitCustomer().getHandphoneNo();
				waNo = orderPartner.getSubmitCustomer().getWaNo();
				domisili = orderPartner.getSubmitDomisili().getAddressDomisili();
				addressDetail = orderPartner.getSubmitDomisili().getAddressDomisili().concat(", ")
						.concat(orderPartner.getSubmitDomisili().getKelurahanDesc()).concat(", ")
						.concat(orderPartner.getSubmitDomisili().getKecamatanDesc()).concat(", ")
						.concat(orderPartner.getSubmitDomisili().getZipcode());
				kelurahan = orderPartner.getSubmitDomisili().getKelurahanDesc().concat(", ")
						.concat(orderPartner.getSubmitDomisili().getKecamatanDesc()).concat(", ")
						.concat(orderPartner.getSubmitDomisili().getZipcode());
				surveyTime = application.getSurveyAppointmentDate() != null
						? secondSdf.format(application.getSurveyAppointmentDate()).concat(" ")
								.concat(formatTime.format(application.getSurveyAppointmentDate()))
						: secondSdf.format(orderPartner.getSubmitDomisili().getSurveyDate()).concat(" ")
								.concat(orderPartner.getSubmitDomisili().getSurveyTime());
				tipeCustomer = orderPartner.getSubmitCustomer().getCustType();
				kotaDomisili = orderPartner.getSubmitUnit().getKotaDomisili();
			}

			if (orderPartner != null) {
				jenisAsuransi = orderPartner.getSubmitUnit().getJenisAsuransi();
				promo = orderPartner.getSubmitUnit().getPromo();
				tahunPembuatan = orderPartner.getSubmitUnit().getObjectYear();
			}

			Optional<ApplicationObject> findUnit = application.getApplicationObject().stream()
					.filter(e -> e.getApplicationNoUnit().equalsIgnoreCase(applNoUnit)).findAny();

			if (findUnit.isPresent()) {
				var applicationObject = findUnit.get();
				objectPrice = applicationObject.getObjectPrice().setScale(0);
				dp = applicationObject.getDpGross() != null ? applicationObject.getDpGross()
						: BigDecimal.ZERO.setScale(0);
				tenor = applicationObject.getApplicationTop();
				installmentAmount = applicationObject.getInstallmentAmt() == null ? BigDecimal.ZERO.setScale(0)
						: applicationObject.getInstallmentAmt();
				rate = applicationObject.getFlatRate() == null ? BigDecimal.ZERO.setScale(0)
						: applicationObject.getFlatRate();
				tipePembiayaan = paraFinTypeRepository
						.findNameById(Integer.valueOf(applicationObject.getFinancingType()));
				objectCode = applicationObject.getObject();

				var paraObject = paraObjectRepository.findByObjectId(objectCode);

				// Get From Collateral Otomotive
				if (applicationObject.getApplicationObjectCollOto() != null) {

					tipeJaminan = objectCode.equalsIgnoreCase("014")
							? jaminanDanaTunai.concat(paraObjectRepository
									.findByObjectId(applicationObject.getApplicationObjectCollOto().getObject())
									.getParaObjectName())
							: paraObjectRepository
									.findByObjectId(applicationObject.getApplicationObjectCollOto().getObject())
									.getParaObjectName();

					merk = paraObjectBrandRepository
							.findByObjectId(applicationObject.getApplicationObjectCollOto().getBrandObject());
					model = paraObjectModelRepository
							.findByObjectId(applicationObject.getApplicationObjectCollOto().getObjectModel());
					tahunPembuatan = String.valueOf(applicationObject.getApplicationObjectCollOto().getMfgYear());
				} else {
					tipeJaminan = paraObject.getParaObjectName();
					merk = paraObjectBrandRepository.findByObjectId(applicationObject.getBrandObject());
					model = paraObjectModelRepository.findByObjectId(applicationObject.getObjectModel());
				}

				// Get JenisInsurance from Application Insurance
				if (applicationObject.getApplicationInsurance() != null
						&& !applicationObject.getApplicationInsurance().isEmpty()) {

					// Filler Hanya Asuransi Utama dan yang aktive yang di get
					var applicationInsurance = applicationObject.getApplicationInsurance().stream()
							.filter(e -> e.getInsuranceType().equalsIgnoreCase("2") && e.isActive() == Boolean.TRUE)
							.collect(Collectors.toList()).get(0);

					// Pengecekan Jika Asuransi Combinasi
					if (!applicationInsurance.getInsuranceType2().isEmpty()) {

						jenisAsuransi = String.valueOf(Integer.valueOf(applicationInsurance.getTenorInsurance1()) / 12)
								.concat(AppConst.TIPE_INSURANCE_COMBI.getMessage());
					} else {
						if (applicationInsurance.getInsuranceType1().equalsIgnoreCase("1")) {
							jenisAsuransi = AppConst.TIPE_INSURANCE_SINGLE_TLO.getMessage();
						}
						if (applicationInsurance.getInsuranceType1().equalsIgnoreCase("2")) {
							jenisAsuransi = AppConst.TIPE_INSURANCE_SINGLE_COMPRE.getMessage();
						}

					}

					// jenisAsuransi
				}

				objectDetail = paraObject.getParaObjectName().concat(", ").concat(merk).concat(" ").concat(model);

			} else {
				if (orderPartner != null) {
					objectPrice = orderPartner.getSubmitUnit().getObjectPrice();
					dp = orderPartner.getSubmitUnit().getDp();
					tenor = orderPartner.getSubmitUnit().getTenor();
					installmentAmount = orderPartner.getSubmitUnit().getInstallmentAmt();
					objectCode = orderPartner.getSubmitUnit().getObject();
					rate = orderPartner.getSubmitUnit().getRate();
					tipeJaminan = orderPartner.getSubmitUnit().getTipeJaminan();
					merk = orderPartner.getSubmitUnit().getObjectBrandDesc();
					objectDetail = orderPartner.getSubmitUnit().getObjectTypeDesc().concat(" ")
							.concat(orderPartner.getSubmitUnit().getObjectBrandDesc()).concat(" ")
							.concat(orderPartner.getSubmitUnit().getObjectModelDesc());
				}

			}

			// set Into DTO Response
			orderDetail.setOrderNo(application.getOrderNo());
			orderDetail.setIdNo(custIdNo);
			orderDetail.setCustomerName(customerName);
			orderDetail.setHandphoneNo(handphoneNo);
			orderDetail.setWaNo(waNo);
			orderDetail.setAddressDomisili(domisili);
			orderDetail.setAddressDetail(addressDetail);
			orderDetail.setKelurahan(kelurahan);
			orderDetail.setSurveyDateTime(surveyTime);
			orderDetail.setObjectDetail(objectDetail);
			orderDetail.setObjectPrice(objectPrice);
			orderDetail.setDp(dp);
			orderDetail.setTenor(tenor);
			orderDetail.setInstallmentAmt(installmentAmount);
			orderDetail.setPromo(promo);
			orderDetail.setObjectCode(objectCode);
			orderDetail.setRate(rate);
			orderDetail.setObjectYear(tahunPembuatan);
			orderDetail.setTipeJaminan(tipeJaminan);
			orderDetail.setJenisAsuransi(jenisAsuransi);
			orderDetail.setMerk(merk);
			orderDetail.setTipePembiayaan(tipePembiayaan);
			orderDetail.setTipeCustomer(tipeCustomer);
			orderDetail.setPicCompany(picCompany);
			orderDetail.setKotaDomisili(kotaDomisili);

			// Get Document Type from Application Document
			if (custType.equalsIgnoreCase(CUST_PERSONAL.getMessage())) {

				List<String> listDocument = application.getApplicationDocument().stream().filter(
						e -> e.getParaDocumentTypeId() != null && e.getParaDocumentTypeId().equalsIgnoreCase("A05"))
						.map(value -> value.getObjectIdEcm()).collect(Collectors.toList());

				objectDocumentId = !listDocument.isEmpty() ? listDocument.get(0) : data;
			} else {
				List<String> listDocument = application.getApplicationDocument().stream().filter(
						e -> e.getParaDocumentTypeId() != null && e.getParaDocumentTypeId().equalsIgnoreCase("A13"))
						.map(value -> value.getObjectIdEcm()).collect(Collectors.toList());

				objectDocumentId = !listDocument.isEmpty() ? listDocument.get(0) : data;
			}

			List<String> listDocument = application.getApplicationDocument().stream()
					.filter(e -> e.getParaDocumentTypeId() != null && e.getParaDocumentTypeId().equalsIgnoreCase("A6M"))
					.map(value -> value.getObjectIdEcm()).collect(Collectors.toList());

			objectDocumentBpkb = !listDocument.isEmpty() ? listDocument.get(0) : data;

			// Hit Service ECM For Document KTP/NPWP
			if (!objectDocumentId.isEmpty()) {
				byte[] documentByte = externalApi.getDocumentEcm(objectDocumentId, "01");
				objectDocumentId = Base64.getEncoder().encodeToString(documentByte);
			}

			// Hit Service ECM For Document BPKB
			if (!objectDocumentBpkb.isEmpty()) {
				byte[] documentByte = externalApi.getDocumentEcm(objectDocumentBpkb, "01");
				objectDocumentBpkb = Base64.getEncoder().encodeToString(documentByte);
			}

			orderDetail.setFotoCustomer(objectDocumentId);
			orderDetail.setFotoSTNK(objectDocumentBpkb);

		} catch (Exception e) {
			// orderDetail = new TrackingOrderDetailResDTO();
			LOGGER.error(ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL.getMessage(), e.getMessage());
		}

		return orderDetail;
	}

	public List<TrackingOrderTagihanResDTO> getListTrackingTagihan(TrackingOrderTagihanReqDTO trackingTagihan) {

		List<TrackingOrderTagihanResDTO> response = new ArrayList<>();

		try {

			response = applicationTrackingRepository.findTagihanByDateAndDlc(trackingTagihan.getStartDate(),
					trackingTagihan.getEndDate(), trackingTagihan.getDealCode(), trackingTagihan.getGroupStatus());

		} catch (Exception e) {
			LOGGER.error("Response Error: {}", e.toString());
		}

		return response;
	}

	public List<TrackingOrderSummaryResDTO> getSummaryTrackingOrder(TrackingOrderWebReqDTO trackingOrderReq) {

		List<TrackingOrderSummaryResDTO> response = new ArrayList<>();
		List<Tuple> tracking = new ArrayList<>();
		var totalDraft = BigInteger.ZERO;

		try {

			if (trackingOrderReq.getUserType().equalsIgnoreCase("1")) {
				tracking = applicationTrackingRepository.findSummaryTrackingOrderSales(trackingOrderReq.getStartDate(),
						trackingOrderReq.getEndDate(), trackingOrderReq.getUserId());

				// trackingOrderDraft
				totalDraft = applicationTrackingRepository.findSummaryDraftByDateAndUserId(
						trackingOrderReq.getStartDate(), trackingOrderReq.getEndDate(), trackingOrderReq.getUserId());

			} else {
				tracking = applicationTrackingRepository.findSummaryTrackingOrderOwner(trackingOrderReq.getStartDate(),
						trackingOrderReq.getEndDate(), trackingOrderReq.getUserId(), trackingOrderReq.getUserType());

				totalDraft = applicationTrackingRepository.findSummaryDraftByDateAndDlc(trackingOrderReq.getStartDate(),
						trackingOrderReq.getEndDate(), trackingOrderReq.getUserId());
			}

			response = tracking.stream()
					.map(e -> new TrackingOrderSummaryResDTO(
							GroupStatusTracking.MAP.sortNumber(e.get("groupStatus", String.class)),
							e.get("groupStatus", String.class), e.get("total", BigInteger.class)))
					.collect(Collectors.toList());

			response.add(new TrackingOrderSummaryResDTO(8, "Draft Order", totalDraft));

			Collections.sort(response, (TrackingOrderSummaryResDTO a, TrackingOrderSummaryResDTO b) -> a.getRowNum()
					.compareTo(b.getRowNum()));

		} catch (Exception e) {
			LOGGER.error("Response Error get Tracking Summary: {}", e.toString());
		}

		return response;
	}

	public List<TrackingOrderSummaryResDTO> getSummaryTagihan(TrackingOrderTagihanReqDTO tagihanRequest) {

		List<TrackingOrderSummaryResDTO> response = new ArrayList<>();
		List<Tuple> tagihan = new ArrayList<>();

		try {

			tagihan = applicationTrackingRepository.findSummaryTagihan(tagihanRequest.getStartDate(),
					tagihanRequest.getEndDate(), tagihanRequest.getDealCode());

			var sudahCair = notifPaymentRepository
					.findAllPartnerCode(tagihanRequest.getDealCode(), tagihanRequest.getStartDate(),
							tagihanRequest.getEndDate())
					.stream().filter(e -> e.getJenisCair().equalsIgnoreCase("KML")
							|| e.getJenisCair().equalsIgnoreCase("PRODUCT"))
					.collect(Collectors.toList());

			response = tagihan.stream()
					.map(e -> new TrackingOrderSummaryResDTO(
							GroupStatusTracking.MAP.sortNumberTagihan(e.get("groupStatus", String.class)),
							e.get("groupStatus", String.class), e.get("total", BigInteger.class)))
					.collect(Collectors.toList());

			// response.add(new TrackingOrderSummaryResDTO(4, "Draft Tagihan",
			// BigInteger.ZERO));

			Collections.sort(response, (TrackingOrderSummaryResDTO a, TrackingOrderSummaryResDTO b) -> a.getRowNum()
					.compareTo(b.getRowNum()));

			response.set(3, new TrackingOrderSummaryResDTO(3, "Sudah Cair", BigInteger.valueOf(sudahCair.size())));

		} catch (Exception e) {
			LOGGER.error("Response Error get Tracking Summary: {}", e.toString());
		}

		return response;
	}

	public List<NotificationResDTO> getNotification(String userId) {
		List<NotificationResDTO> response = new ArrayList<>();

		try {
			response = notifrepository.findNotificationByUserId(userId);
		} catch (Exception e) {
			LOGGER.error("Response Error get List Notification: {}", e.toString());
		}

		return response;
	}

	public void updateNotification(String notifId) {

		try {
			notifrepository.updateNotifById(notifId);
		} catch (Exception e) {
			LOGGER.error("Response Error Update Notification: {}", e.toString());
		}

	}

	public String getFilterDate(int flag) {
		var localDate = LocalDate.now();
		String fillterDate = localDate.minusWeeks(3).format(formatDate);
		if (flag == 1) {
			fillterDate = localDate.minusDays(3).format(formatDate);
		} else if (flag == 2) {
			fillterDate = localDate.minusWeeks(1).format(formatDate);
		} else if (flag == 3) {
			fillterDate = localDate.minusWeeks(2).format(formatDate);
		}
		return fillterDate;
	}

	public byte[] fileEcmPo(PoEcmReqDTO poRequest) {

		byte[] document = null;

		try {

			var findObjectId = poRepository.findById(poRequest.getSkPo());

			var objectId = data;

			if (findObjectId.isPresent())
				objectId = findObjectId.get().getDocumentLink().replaceAll("[{}]+", "");

			document = externalApi.getDocumentEcm(objectId, "");

		} catch (Exception e) {
			LOGGER.error("Response Error get PO: {}", e.toString());
		}

		return document;
	}

	public List<ListOrderPencairanResDTO> getListPencairan(PembiayaanReqDTO pembiayaanRequest) {

		List<ListOrderPencairanResDTO> response = new ArrayList<>();
		try {

			if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("009")) {
				List<ListOrderPencairanResDTO> findUnit = notifPaymentRepository.findByKedayIdAndProductId(
						pembiayaanRequest.getPartnerId(), pembiayaanRequest.getStartDate(),
						pembiayaanRequest.getEndDate());

				if (pembiayaanRequest.getProductType().equals("1")) {
					response = findUnit.stream().filter(
							e -> e.getJenisCair().equalsIgnoreCase("001") || e.getJenisCair().equalsIgnoreCase("002"))
							.collect(Collectors.toList());
					response.stream().forEach(e -> e.setJenisCair("OTOMOTIVE"));
				} else if ((pembiayaanRequest.getProductType().equals("2"))) {
					response = findUnit.stream().filter(e -> e.getJenisCair().equalsIgnoreCase("003"))
							.collect(Collectors.toList());
					response.stream().forEach(e -> e.setJenisCair("DURABLE"));
				} else if ((pembiayaanRequest.getProductType().equals("3"))) {
					response = findUnit.stream().filter(e -> e.getJenisCair().equalsIgnoreCase("007"))
							.collect(Collectors.toList());
					response.stream().forEach(e -> e.setJenisCair("DANA TUNAI"));
				} else {
					findUnit.stream().forEach(e -> {
						if (e.getJenisCair().equalsIgnoreCase("001") || e.getJenisCair().equalsIgnoreCase("002")) {
							e.setJenisCair("OTOMOTIVE");
						} else if (e.getJenisCair().equalsIgnoreCase("003")) {
							e.setJenisCair("DURABLE");
						} else {
							e.setJenisCair("DANA TUNAI");
						}
					});
					response = findUnit;
				}

			} else if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("001")
					|| (pembiayaanRequest.getPartnerType().equalsIgnoreCase("002"))) {
				response = notifPaymentRepository
						.findAllPartnerCode(pembiayaanRequest.getPartnerId(), pembiayaanRequest.getStartDate(),
								pembiayaanRequest.getEndDate())
						.stream().filter(e -> e.getJenisCair().equalsIgnoreCase("KML")
								|| e.getJenisCair().equalsIgnoreCase("PRODUCT"))
						.collect(Collectors.toList());
			} else {
				response = notifPaymentRepository.findAllPartnerByAxi(pembiayaanRequest.getPartnerId(),
						pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate());
//						.stream().filter(e -> e.getJenisCair().equalsIgnoreCase("KOMISI")
//								|| e.getJenisCair().equalsIgnoreCase("PRODUCT"))
//						.collect(Collectors.toList());
			}

			if (!pembiayaanRequest.getBranchNo().equalsIgnoreCase("ALL")) {
				response = response.stream()
						.filter(e -> e.getBranchId().equalsIgnoreCase(pembiayaanRequest.getBranchNo()))
						.collect(Collectors.toList());
			}
			response.stream()
					.forEach(e -> e.setObjectDesc(paraObjectModelRepository.findByObjectId(e.getObjectDesc())));
		} catch (Exception e) {
			LOGGER.error("Response Error get List Pencairan: {}", e.toString());
		}
		return response;

	}

	public List<SummaryHeaderPencairanResDTO> getSummaryPencairan(PembiayaanReqDTO pembiayaanRequest) {

		List<SummaryHeaderPencairanResDTO> response = new ArrayList<>();
		List<Tuple> pencairan = new ArrayList<>();
		try {

			if (pembiayaanRequest.getBranchNo().equalsIgnoreCase("ALL")) {
				if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("009")) {

					pencairan = notifPaymentRepository.findSummaryKedayById(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate());

				} else if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("001")
						|| (pembiayaanRequest.getPartnerType().equalsIgnoreCase("002"))) {
					pencairan = notifPaymentRepository.findSummaryDealerById(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate());
				} else {
					pencairan = notifPaymentRepository.findSummaryAxiById(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate());

				}
			} else {
				if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("009")) {

					pencairan = notifPaymentRepository.findSummaryKedayByIdAndCabang(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate(),
							pembiayaanRequest.getBranchNo());

				} else if (pembiayaanRequest.getPartnerType().equalsIgnoreCase("001")
						|| (pembiayaanRequest.getPartnerType().equalsIgnoreCase("002"))) {
					pencairan = notifPaymentRepository.findSummaryDealerByIdAndCabang(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate(),
							pembiayaanRequest.getBranchNo());
				} else {
					pencairan = notifPaymentRepository.findSummaryAxiByIdAndCabang(pembiayaanRequest.getPartnerId(),
							pembiayaanRequest.getStartDate(), pembiayaanRequest.getEndDate(),
							pembiayaanRequest.getBranchNo());

				}
			}
			response = pencairan.stream().map(e -> new SummaryHeaderPencairanResDTO(e.get("status", String.class),
					e.get("jumlahPencairan", BigDecimal.class))).collect(Collectors.toList());

		} catch (Exception e) {
			LOGGER.error("Response Error get Summary Pencairan: {}", e.toString());
		}
		return response;
	}

	public List<CabangPencairanResDTO> getListCabangPencairan(String partnerCode) {
		List<CabangPencairanResDTO> response = new ArrayList<>();
		try {
			response = notifPaymentRepository.findAllBranchPencairan(partnerCode);
		} catch (Exception e) {
			LOGGER.error("Response Error get All Cabang Pencairan: {}", e.toString());
		}
		return response;
	}
}