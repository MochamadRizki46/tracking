package id.co.adira.partner.tracking.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * @author 10999943
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TrackingOrderHeaderReqDTO {
	@NotEmpty(message = "Order number must be filled!")
	private String orderNo;
	@NotEmpty(message = "Appl number payung must be filled!")
	private String applPayung;
	@NotEmpty(message = "Appl number unit must be filled!")
	private String applNoUnit;

}