package id.co.adira.partner.tracking.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusConst {

    OK("Ok"),
    SUCCESS("Success"),
    CANNOT_FIND_SUBMIT_ORDER_WITH_ID("Can't find submit order with id.co: "),
    APPL_NO(""),
    SISTEM_MENERIMA_ORDER("Sistem Menerima Order"),
    ORDER_TERKIRIM("Order Terkirim"),
    SVR("SVR"),
    THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM("There's something error in our system!"),
    VERIFIKASI_SEDANG_DILAKUKAN("Verifikasi sedang dilakukan"),
    ERROR_SENDING_ORDER_TO_LEAD_ENGINE("Error Send order to Lead Engine"),
    AD1PARTNER("Ad1partner"),
    FAILED_GET_KECAMATAN("Failed Get Kecamatan "),
    FAILED_GET_MODEL("Failed Get Model "),
    FAILED_GET_KABKOT("Failed Get Kabkot "),
    FAILED_TO_GET_TRACKING_HEADER_APPL("Failed Get Tracking Order Header by Appl: {} "),
    FAILED_TO_GET_TRACKING_HEADER_PARTNER("Failed Get Tracking Order Header by Partner: {} "),
    FAILED_TO_DELETE_DRAFT("Failed to delete draft: "),
    INSERT_TRACKING_HISTORY_ORDER_TERKIRIM("Insert Tracking History Order Terkirim"),
    VERIFIKASI_ORDER("Verifikasi Order"),
    GET_DATA_FROM_TABLE_SUBMIT_ORDER("Get Data from table submit order"),
    INSERT_TRACKING_HISTORY_VERIFIKASI_ORDER("Insert Tracking History Verifikasi Order"),
    GET_TRACKING_HEADER("Get Tracking Header"),
    FIRST_TRACKING_HOME("tracking home 1"),
    SECOND_TRACKING_HOME("tracking home 2 {}"),
    THIRD_TRACKING_HOME("tracking home 3"),
    DATA_TRACKING_NOT_FOUND_ON_LE("Data Tracking Detail Not Found From LE"),
    CANNOT_FIND_SUBMIT_ORDER_WITH_APP_NO_UNIT("Can't find submit order with app no unit: "),
    CANNOT_FIND_SUBMIT_CUSTOMER_WITH_CURRENT_SUBMIT_ORDER("Can't find submit customer with current submit order. Cause: {}"),
    CANNOT_FIND_SUBMIT_ORDER_WITH_ORDER_NO("Can't find submit order with order no: "),
    FAILED_TO_SAVE_DATA_TO_DB("Failed to save current data to db: {}"),
    CANNOT_PARSE_DATE("Can't parse date: "),
    ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL("Error while mapping tracking order detail: {}"),
    FAILED_TO_CONVERT_FILE("Failed to convert file: {}"),
    ERROR_WHILE_HITTING_UPLOAD_ECM("Error while hitting Upload ECM Endpoint: "),
    ERROR_WHILE_HITTING_DELETE_DRAFT("Error while hitting Delete Draft Endpoint: "),
    ERROR_WHILE_HITTING_URL_APPL_NO("Error while hitting URL Appl No: "),
    ERROR_WHILE_HITTING_EP_TRACKING("Error while hitting URL EP Tracking: "),
    ERROR_WHILE_HITTING_URL_HISTORY("Error while hitting URL History: "),
    ERROR_WHILE_HITTING_URL_EP_HISTORY("Error while hitting URL EP History: ");

    private String message;

}
