package id.co.adira.partner.tracking.entity.params;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "para_object_brand")
@Data
public class ParaObjectBrand {

	@Id
	@Column(name = "para_object_group_id")
	private String paraObjectGroupId;
	@Column(name = "para_object_made_in_id")
	private String paraObjectMadeInId;
	@Column(name = "para_object_brand_id")
	private String paraObjectBrandId;
	@Column(name = "para_object_brand_name")
	private String paraObjectBrandName;
	@Column(name = "effective_date")
	private Date effectiveDate;
	@Column(name = "active")
	private int active;
	@Column(name = "logid")
	private BigDecimal logid;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "modified_date")
	private Date modifiedDate;
	@Column(name = "modified_by")
	private String modifiedBy;
	@Column(name = "visible")
	private int visible;
}