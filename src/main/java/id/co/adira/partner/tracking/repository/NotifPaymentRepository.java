/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import java.util.List;

import javax.persistence.Tuple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.tracking.dto.responses.CabangPencairanResDTO;
import id.co.adira.partner.tracking.dto.responses.ListOrderPencairanResDTO;
import id.co.adira.partner.tracking.entity.NotifPayment;

/**
 * @author Vionza
 *
 */
@Repository
public interface NotifPaymentRepository extends JpaRepository<NotifPayment, String>{

	
	@Query(" select new id.co.adira.partner.tracking.dto.responses.ListOrderPencairanResDTO("
			+ "to_char(n.tanggalCair, 'dd mon yyyy') as tanggalCair, "
			+ "ao.groupObject, "
			+ "n.docInvoiceNo, "
			+ "n.namaRekeningPenerima, "
			+ "n.noRekeningPenerima, "
			+ "case when c.customerType = 'PER' then cp.identityFullName else cc.companyName end as custName, "
			+ "ao.objectModel as objectDesc, "
			+ "(select cp.branchName from ParaCpUnit cp where cp.branchId = n.branchNo and cp.active = 0) as branchName,"
			+ "n.applicationNo, "
			+ "n.jumlahPencairan, "
			+ "to_number('0', '9') as jumlahKomisi, "
			+ "n.branchNo) "
			+ "from NotifPayment n, Application a, ApplicationObject ao, Customer c "
			+ "left join  CustomerPersonal cp on c.skCustomer = cp.fkCustomer "
			+ "left join CustomerCompany cc on c.skCustomer = cc.fkCustomer "
			+ "where a.skApplication = ao.fkApplication "
			+ "and a.fkCustomer = c.customerId "
			+ "and n.applicationNo = a.applicationNo "
			+ "and n.bpPartner = :partnerCode "
			+ "and n.jenisCair = 'PRODUCT' "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') ")
	List<ListOrderPencairanResDTO> findByKedayIdAndProductId(@Param("partnerCode") String partnerCode, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	@Query(" select new id.co.adira.partner.tracking.dto.responses.ListOrderPencairanResDTO("
			+ "to_char(n.tanggalCair, 'dd mon yyyy') as tanggalCair, "
			+ "n.jenisCair, "
			+ "n.docInvoiceNo, "
			+ "n.namaRekeningPenerima, "
			+ "n.noRekeningPenerima, "
			+ "case when c.customerType = 'PER' then cp.identityFullName else cc.companyName end as custName, "
			+ "ao.objectModel as objectDesc, "
			+ "(select cp.branchName from ParaCpUnit cp where cp.branchId = n.branchNo and cp.active = 0) as branchName,"
			+ "n.applicationNo, "
			+ "n.jumlahPencairan, "
			+ "to_number('0', '9') as jumlahKomisi, "
			+ "n.branchNo) "
			+ "from NotifPayment n, Application a, ApplicationObject ao, Customer c "
			+ "left join  CustomerPersonal cp on c.skCustomer = cp.fkCustomer "
			+ "left join CustomerCompany cc on c.skCustomer = cc.fkCustomer "
			+ "where a.skApplication = ao.fkApplication "
			+ "and  a.fkCustomer = c.customerId "
			+ "and n.applicationNo = a.applicationNo "
			+ "and n.bpPartner = :partnerCode "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') ")
	List<ListOrderPencairanResDTO> findAllPartnerCode(@Param("partnerCode") String partnerCode, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	@Query(" select new id.co.adira.partner.tracking.dto.responses.ListOrderPencairanResDTO("
			+ "to_char(n.tanggalCair, 'dd mon yyyy') as tanggalCair, "
			+ "'' as jenisCair, "
			+ "'' as docInvoiceNo, "
			+ "'' as namaRekeningPenerima, "
			+ "'' as noRekeningPenerima, "
			+ "case when c.customerType = 'PER' then cp.identityFullName else cc.companyName end as custName, "
			+ "ao.objectModel as objectDesc, "
			+ "'' as branchName, "
			+ "n.applicationNo, "
			+ "coalesce((select np.jumlahPencairan from NotifPayment np where np.bpPartner = :partnerCode and np.applicationNo = n.applicationNo and np.jenisCair = 'PRODUCT'), 0) as jumlahPencairan, "
			+ "(select coalesce(np1.jumlahPencairan, to_number('0','9')) from NotifPayment np1 where np1.bpPartner = :partnerCode and np1.applicationNo = n.applicationNo and np1.jenisCair = 'KOMISI') as jumlahKomisi, "
			+ "n.branchNo as branchId) "
			+ "from NotifPayment n, Application a, ApplicationObject ao, Customer c "
			+ "left join  CustomerPersonal cp on c.skCustomer = cp.fkCustomer "
			+ "left join CustomerCompany cc on c.skCustomer = cc.fkCustomer "
			+ "where a.skApplication = ao.fkApplication "
			+ "and  a.fkCustomer = c.customerId "
			+ "and n.applicationNo = a.applicationNo "
			+ "and n.bpPartner = :partnerCode "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ "and to_date(to_char(n.tanggalCair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ "group by n.applicationNo, n.tanggalCair, n.branchNo, custName, objectDesc ")
	List<ListOrderPencairanResDTO> findAllPartnerByAxi(@Param("partnerCode") String partnerCode, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	@Query(nativeQuery = true, value = "select "
			+ "rownum, "
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Total Semua Pembiayaan' as status,"
			+ " coalesce (coalesce (sum(a.amt_cair), 0), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idKeday "
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Pembiayaan Otomotif' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where "
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object in ('001', '002')"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 3 as rownum,"
			+ " 'Total Pembiayaan Dana Tunai' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where"
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object = '007'"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 4 as rownum,"
			+ " 'Total Pembiayaan Lainya' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where"
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object = '003'"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT'"
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryKedayById(@Param("idKeday") String idKeday, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	@Query(nativeQuery = true, value = "select"
			+ " rownum,"
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Total Pencairan Unit' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idDealer "
			+ " and a.jenis_cair = 'PRODUCT'"
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Komisi Langsung' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idDealer"
			+ " and a.jenis_cair = 'KML'"
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryDealerById(@Param("idDealer") String idDealer, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	@Query(nativeQuery = true, value = "select"
			+ " rownum,"
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Pembiayaan Pribadi' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Pembiayaan Rekan' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi"
			+ " and a.jenis_cair = 'KOMISI' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Insentif' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi"
			+ " and a.jenis_cair = 'INSENTIF' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryAxiById(@Param("idAxi") String idAxi, @Param("startDate") String startDate, 
			@Param("endDate") String endDate);
	
	
	@Query(nativeQuery = true, value = "select "
			+ "rownum, "
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Total Semua Pembiayaan' as status,"
			+ " coalesce (coalesce (sum(a.amt_cair), 0), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idKeday "
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Pembiayaan Otomotif' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where "
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object in ('001', '002')"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 3 as rownum,"
			+ " 'Total Pembiayaan Dana Tunai' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where"
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object = '007'"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 4 as rownum,"
			+ " 'Total Pembiayaan Lainya' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a,"
			+ " application b,"
			+ " application_object c"
			+ " where"
			+ " a.appl_no = b.application_no"
			+ " and b.sk_application = c.fk_application"
			+ " and c.group_object = '003'"
			+ " and a.bp_ext = :idKeday"
			+ " and a.jenis_cair = 'PRODUCT'"
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryKedayByIdAndCabang(@Param("idKeday") String idKeday, @Param("startDate") String startDate, 
			@Param("endDate") String endDate, @Param("cabang") String cabang);
	
	@Query(nativeQuery = true, value = "select"
			+ " rownum,"
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Total Pencairan Unit' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idDealer "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Komisi Langsung' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idDealer"
			+ " and a.jenis_cair = 'KML'"
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryDealerByIdAndCabang(@Param("idDealer") String idDealer, @Param("startDate") String startDate, 
			@Param("endDate") String endDate, @Param("cabang") String cabang);
	
	@Query(nativeQuery = true, value = "select"
			+ " rownum,"
			+ " status,"
			+ " jumlahPencairan " 
			
			+ "from"
			+ " ("
			+ " select"
			+ " 1 as rownum,"
			+ " 'Pembiayaan Pribadi' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Pembiayaan Rekan' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi"
			+ " and a.jenis_cair = 'KOMISI' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ "union"
			+ " select"
			+ " 2 as rownum,"
			+ " 'Total Insentif' as status,"
			+ " coalesce (sum(a.amt_cair), 0) as jumlahPencairan" 
			
			+ " from"
			+ " sap_notif_payment a"
			+ " where"
			+ " a.bp_ext = :idAxi"
			+ " and a.jenis_cair = 'PRODUCT' "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') >= to_date(:startDate, 'dd-mm-yyyy') "
			+ " and to_date(to_char(a.tanggal_cair, 'dd-mm-yyyy'), 'dd-mm-yyyy') <= to_date(:endDate, 'dd-mm-yyyy') "
			+ " and a.appl_br_id = :cabang "
			+ ") list"
			+ " order by"
			+ " rownum ")
	List<Tuple> findSummaryAxiByIdAndCabang(@Param("idAxi") String idAxi, @Param("startDate") String startDate, 
			@Param("endDate") String endDate, @Param("cabang") String cabang);
	
	@Query("select new id.co.adira.partner.tracking.dto.responses.CabangPencairanResDTO(n.branchNo, cp.branchName) "
			+ "from NotifPayment n, ParaCpUnit cp "
			+ "where n.branchNo = cp.branchId "
			+ "and cp.active = 0 "
			+ "and n.bpPartner = :partnerCode "
			+ "group by n.branchNo, cp.branchName")
	List<CabangPencairanResDTO> findAllBranchPencairan(@Param("partnerCode") String partnerCode);
	
}




