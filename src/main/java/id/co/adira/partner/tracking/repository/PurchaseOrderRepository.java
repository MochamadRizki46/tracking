/**
 * 
 */
package id.co.adira.partner.tracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.adira.partner.tracking.entity.PurchaseOrder;

/**
 * @author Vionza
 *
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, String> {

}
