/**
 * 
 */
package id.co.adira.partner.tracking.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
public class ApplicationLoanDTO {
	private String applId;
	private long installmentNo;
	private BigDecimal percentage;
	private BigDecimal amount;
	private boolean active;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private String fkApplicationObjt;
}
