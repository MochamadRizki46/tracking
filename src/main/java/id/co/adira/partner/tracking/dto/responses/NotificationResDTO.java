/**
 * 
 */
package id.co.adira.partner.tracking.dto.responses;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * @author Vionza
 *
 */

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NotificationResDTO {

	private String notifId;
	private String orderNo;
	private String applicationNo;
	private String applicationNoUnit;
	private String title;
	private String body;
	private String status;
	private Boolean isRead;
	private String createdDate;
	private String createdTime;
	private String userId;
}
