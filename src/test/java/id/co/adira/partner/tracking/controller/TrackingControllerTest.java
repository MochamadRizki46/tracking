package id.co.adira.partner.tracking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.tracking.dto.request.*;
import id.co.adira.partner.tracking.dto.responses.*;
import id.co.adira.partner.tracking.entity.Application;
import id.co.adira.partner.tracking.entity.Customer;
import id.co.adira.partner.tracking.entity.SubmitOrder;
import id.co.adira.partner.tracking.repository.ApplicationRepository;
import id.co.adira.partner.tracking.repository.CustomerRepository;
import id.co.adira.partner.tracking.repository.SubmitOrderRepository;
import id.co.adira.partner.tracking.service.TrackingService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class TrackingControllerTest {

    @InjectMocks
    TrackingController trackingController;

    @Mock
    TrackingService trackingService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Mock
    ApplicationRepository applicationRepository;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    SubmitOrderRepository submitOrderRepository;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTrackingHeaderAndExpectStatusOk() throws Exception {
        TrackingOrderHeaderReqDTO trackingOrderHeaderReqDTO = new TrackingOrderHeaderReqDTO("A1P0000SLD2100045382", "APPL001", "APPU001");

        TrackingOrderHeaderResDTO trackingOrderHeaderResDTO = new TrackingOrderHeaderResDTO(
                "A1P0000SLD2100045382", "APPL001", "test_customer", "2022-02-07",
                "Draft", "OBJ001",
                "test_object_desc", "OBM001",
                 "test_object_model", "dummy_nama_so",
                "2022-02-07", "18:00",
                "10083392");


        Application dummyApplication = mapApplication();
        Customer dummyCustomer = mapCustomer();
        SubmitOrder dummySubmitOrder = mapSubmitOrder();

        when(trackingService.getTrackingOrderHeader(trackingOrderHeaderReqDTO)).thenReturn(trackingOrderHeaderResDTO);
        when(applicationRepository.findByNoApplication("APPL001")).thenReturn(dummyApplication);
        when(customerRepository.findByCustomerId("CUS001")).thenReturn(dummyCustomer);
        when(submitOrderRepository.findByOrderNo("A1P0000SLD2100045382")).thenReturn(dummySubmitOrder);

        String jsonStr = objectMapper.writeValueAsString(trackingOrderHeaderReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingheader")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGetTrackingHistoryAndExpectStatusOk() throws Exception {
        TrackingOrderHistoryReqDTO trackingOrderHistoryReqDTO = new TrackingOrderHistoryReqDTO();
        trackingOrderHistoryReqDTO.setApplNo("APPL001");
        trackingOrderHistoryReqDTO.setTypeUser("1");

        List<TrackingOrderHistoryResDTO> trackingOrderHistoryResDTOS = new ArrayList<>();

        TrackingOrderHistoryResDTO trackingOrderHistoryResDTO = new TrackingOrderHistoryResDTO("APPL001", "A1P0000SLD2100045382",
                "available", "ok",
                "Draft", "",
                "GRP001", 1);

        trackingOrderHistoryResDTOS.add(trackingOrderHistoryResDTO);

        when(trackingService.getTrackingOrderHistorySls(trackingOrderHistoryReqDTO)).thenReturn(trackingOrderHistoryResDTOS);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderHistoryReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackinghistory")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

//    @Test
//    public void testGetTrackingOrderSlsHomeAndExpectStatusOk() throws Exception {
//        TrackingOrderListReqDTO trackingOrderListReqDTO = new TrackingOrderListReqDTO();
//        trackingOrderListReqDTO.setUserId("user@ad1gate.com");
//        trackingOrderListReqDTO.setFlag(0);
//        trackingOrderListReqDTO.setUserType("1");
//        trackingOrderListReqDTO.setSort(1);
//
//        List<TrackingOrderHomeResDTO> trackingOrderHomeResDTOS = new ArrayList<>();
//
//        TrackingOrderHomeResDTO trackingOrderHomeResDTO = new TrackingOrderHomeResDTO("A1P0000SLD2100045382", "APPL001",
//                "APPU001", "dummy_customer",
//                "2022-02-07", "Draft",
//                "dummy_object", "dummy_object_model",
//                "dummy_group_status", "dummy_branch_name",
//                "dummy_sk_po");
//
//        trackingOrderHomeResDTOS.add(trackingOrderHomeResDTO);
//
//        when(trackingService.getListTrackingHomeSls(trackingOrderListReqDTO)).thenReturn(trackingOrderHomeResDTOS);
//        String jsonStr = objectMapper.writeValueAsString(trackingOrderListReqDTO);
//
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingorderhome")
//                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);
//
//        mockMvc.perform(requestBuilder).andExpect(status().isOk());
//
//    }
//
//    @Test
//    public void testGetTrackingOrderHomeOwnAndExpectStatusOk() throws Exception {
//        TrackingOrderListReqDTO trackingOrderListReqDTO = new TrackingOrderListReqDTO();
//        trackingOrderListReqDTO.setUserId("user@ad1gate.com");
//        trackingOrderListReqDTO.setFlag(0);
//        trackingOrderListReqDTO.setUserType("2");
//        trackingOrderListReqDTO.setSort(1);
//
//        List<TrackingOrderHomeResDTO> trackingOrderHomeResDTOS = new ArrayList<>();
//
//        TrackingOrderHomeResDTO trackingOrderHomeResDTO = new TrackingOrderHomeResDTO("A1P0000SLD2100045382", "APPL001",
//                "APPU001", "dummy_customer",
//                "2022-02-07", "Draft",
//                "dummy_object", "dummy_object_model",
//                "dummy_group_status", "dummy_branch_name",
//                "dummy_sk_po");
//
//        trackingOrderHomeResDTOS.add(trackingOrderHomeResDTO);
//
//        when(trackingService.getListTrackingHomeSls(trackingOrderListReqDTO)).thenReturn(trackingOrderHomeResDTOS);
//        String jsonStr = objectMapper.writeValueAsString(trackingOrderListReqDTO);
//
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingorderhome")
//                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);
//
//        mockMvc.perform(requestBuilder).andExpect(status().isOk());
//
//    }

    @Test
    public void testGetOrderDetailAndExpectStatusOk() throws Exception {
        TrackingOrderDetailReqDTO trackingOrderDetailReqDTO = new TrackingOrderDetailReqDTO();
        trackingOrderDetailReqDTO.setOrderNo("A1P0000SLD2100045382");
        trackingOrderDetailReqDTO.setApplNo("APPL001");
        trackingOrderDetailReqDTO.setApplNoUnit("APPU001");

        TrackingOrderDetailResDTO trackingOrderDetailResDTO = new TrackingOrderDetailResDTO(
                "A1P0000SLD2100045382", "3670000000000000",
                "dummy_customer_name", "081234567890",
                "081234567890", "dummy_address_domisili",
                "dummy_address_detail", "dummy_kelurahan", "2022-02-07 18:00:00",
                "dummy_object_detail", new BigDecimal(1000000),
                new BigDecimal(200000), 12,
                new BigDecimal(10000000), "dummy_promo",
                "OBC001", new BigDecimal(5),
                "2016", "dummy_tipe_jaminan",
                "dummy_jenis_asuransi", "test",
                "test", "YAHAMA", "PER", "Sandro", "KONVENSIONAL","JAKARTA");

        when(trackingService.getTrackingOrderDetail(trackingOrderDetailReqDTO)).thenReturn(trackingOrderDetailResDTO);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderDetailReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingorderdetail")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGetTrackingOrderTagihanAndExpectStatusOk() throws Exception {
        TrackingOrderTagihanReqDTO trackingOrderTagihanReqDTO = new TrackingOrderTagihanReqDTO();
        trackingOrderTagihanReqDTO.setStartDate("2022-02-05");
        trackingOrderTagihanReqDTO.setEndDate("2022-02-07");
        trackingOrderTagihanReqDTO.setDealCode("DL001");

        List<TrackingOrderTagihanResDTO> trackingOrderTagihanResDTOS = new ArrayList<>();
        TrackingOrderTagihanResDTO firstTrackingOrderTagihan = new TrackingOrderTagihanResDTO("2022-02-05", "APPU001",
                "BRC001", "dummy_customer",
                "dummy_object_model", new BigDecimal(1000000),
                "2022-02-05", "Ok", "group_1",
                "SKPO001");

        trackingOrderTagihanResDTOS.add(firstTrackingOrderTagihan);

        TrackingOrderTagihanResDTO secondTrackingOrderTagihan = new TrackingOrderTagihanResDTO("2022-02-05", "APPU002",
                "BRC002", "dummy_customer",
                "dummy_object_model", new BigDecimal(1000000),
                "2022-02-06", "Ok", "group_2",
                "SKPO002");

        trackingOrderTagihanResDTOS.add(secondTrackingOrderTagihan);

        when(trackingService.getListTrackingTagihan(trackingOrderTagihanReqDTO)).thenReturn(trackingOrderTagihanResDTOS);

        String jsonStr = objectMapper.writeValueAsString(trackingOrderTagihanReqDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingordertagihan")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testCetakPoAndExpectStatusOk() throws Exception {
        PoEcmReqDTO poEcmReqDTO = new PoEcmReqDTO();
        poEcmReqDTO.setSkPo("SKPO001");

        String test = "test";
        byte[] dummyPoByte = test.getBytes(StandardCharsets.UTF_8);


        when(trackingService.fileEcmPo(poEcmReqDTO)).thenReturn(dummyPoByte);
        String jsonStr = objectMapper.writeValueAsString(poEcmReqDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/printPo")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGetTrackingOrderWebSlsAndExpectStatusOk() throws Exception {
        TrackingOrderWebReqDTO trackingOrderWebReqDTO = new TrackingOrderWebReqDTO();
        trackingOrderWebReqDTO.setUserId("user@ad1gate.com");
        trackingOrderWebReqDTO.setUserType("1");
        trackingOrderWebReqDTO.setStartDate("2022-02-05");
        trackingOrderWebReqDTO.setEndDate("2022-02-07");
        trackingOrderWebReqDTO.setGroupStatus("Ok");

        List<TrackingOrderHomeResDTO> trackingOrderHomeResDTOS = new ArrayList<>();

        TrackingOrderHomeResDTO trackingOrderHomeResDTO = new TrackingOrderHomeResDTO("A1P0000SLD2100045382", "APPL001",
                "APPU001", "dummy_customer",
                "2022-02-07", "Draft",
                "dummy_object", "dummy_object_model",
                "dummy_group_status", "dummy_branch_name",
                "dummy_sk_po");

        trackingOrderHomeResDTOS.add(trackingOrderHomeResDTO);

        when(trackingService.getTrackingOrderWebSales(trackingOrderWebReqDTO)).thenReturn(trackingOrderHomeResDTOS);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderWebReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingorderweb")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testGetTrackingWebOwnAndExpectStatusOk() throws Exception {
        TrackingOrderWebReqDTO trackingOrderWebReqDTO = new TrackingOrderWebReqDTO();
        trackingOrderWebReqDTO.setUserId("user@ad1gate.com");
        trackingOrderWebReqDTO.setUserType("2");
        trackingOrderWebReqDTO.setStartDate("2022-02-05");
        trackingOrderWebReqDTO.setEndDate("2022-02-07");
        trackingOrderWebReqDTO.setGroupStatus("Ok");

        List<TrackingOrderHomeResDTO> trackingOrderHomeResDTOS = new ArrayList<>();

        TrackingOrderHomeResDTO trackingOrderHomeResDTO = new TrackingOrderHomeResDTO("A1P0000SLD2100045382", "APPL001",
                "APPU001", "dummy_customer",
                "2022-02-07", "Draft",
                "dummy_object", "dummy_object_model",
                "dummy_group_status", "dummy_branch_name",
                "dummy_sk_po");

        trackingOrderHomeResDTOS.add(trackingOrderHomeResDTO);

        when(trackingService.getTrackingOrderWebOwner(trackingOrderWebReqDTO)).thenReturn(trackingOrderHomeResDTOS);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderWebReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/trackingorderweb")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testGetSummaryAndExpectStatusOk() throws Exception {
        TrackingOrderWebReqDTO trackingOrderWebReqDTO = new TrackingOrderWebReqDTO();
        trackingOrderWebReqDTO.setUserId("user@ad1gate.com");
        trackingOrderWebReqDTO.setUserType("2");
        trackingOrderWebReqDTO.setStartDate("2022-02-05");
        trackingOrderWebReqDTO.setEndDate("2022-02-07");
        trackingOrderWebReqDTO.setGroupStatus("Ok");

        List<TrackingOrderSummaryResDTO> trackingOrderSummaryResDTOS = new ArrayList<>();

        TrackingOrderSummaryResDTO trackingOrderSummaryResDTO = new TrackingOrderSummaryResDTO(1, "group_status", new BigInteger(String.valueOf(1000000)));
        trackingOrderSummaryResDTOS.add(trackingOrderSummaryResDTO);

        when(trackingService.getSummaryTrackingOrder(trackingOrderWebReqDTO)).thenReturn(trackingOrderSummaryResDTOS);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderWebReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/summarytrackingorder")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGetSummaryTagihanAndExpectStatusOk() throws Exception {
        TrackingOrderTagihanReqDTO trackingOrderTagihanReqDTO = new TrackingOrderTagihanReqDTO();
        trackingOrderTagihanReqDTO.setGroupStatus("dummy_group_status");
        trackingOrderTagihanReqDTO.setEndDate("2022-02-10");
        trackingOrderTagihanReqDTO.setStartDate("2022-02-08");
        trackingOrderTagihanReqDTO.setDealCode("dummy_deal_code");

        List<TrackingOrderSummaryResDTO> trackingOrderSummaryResDTOS = new ArrayList<>();

        TrackingOrderSummaryResDTO trackingOrderSummaryResDTO = new TrackingOrderSummaryResDTO();
        trackingOrderSummaryResDTO.setTotal(new BigInteger(String.valueOf(1)));
        trackingOrderSummaryResDTO.setGroupStatus("dummy_group_status");
        trackingOrderSummaryResDTO.setRowNum(1);

        trackingOrderSummaryResDTOS.add(trackingOrderSummaryResDTO);

        when(trackingService.getSummaryTagihan(trackingOrderTagihanReqDTO)).thenReturn(trackingOrderSummaryResDTOS);
        String jsonStr = objectMapper.writeValueAsString(trackingOrderTagihanReqDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/summarytagihan")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }
    


//    @Test
//    public void testGetSummaryPencairanAndExpectStatusOk() throws Exception {
//    	PembiayaanReqDTO pembiayaanReqDTO = new PembiayaanReqDTO();
//    	pembiayaanReqDTO.setPartnerType("001");
//    	pembiayaanReqDTO.setPartnerId("000195");
//    	pembiayaanReqDTO.setBranchNo("ALL");
//    	pembiayaanReqDTO.setProductType("001");
//    	pembiayaanReqDTO.setStartDate("2022-02-08");
//    	pembiayaanReqDTO.setEndDate("2022-02-10");
//
//    	List<SummaryHeaderPencairanResDTO> summaryHeaderPencairanResDTOs = new ArrayList<>();
//
//    	SummaryHeaderPencairanResDTO summaryHeaderPencairanResDTO = new SummaryHeaderPencairanResDTO();
//    	summaryHeaderPencairanResDTO.setTotalPencairan(new BigDecimal(String.valueOf(20000)));
//    	summaryHeaderPencairanResDTO.setStatus("dummy_status");
//
//    	summaryHeaderPencairanResDTOs.add(summaryHeaderPencairanResDTO);
//
//        when(trackingService.getSummaryPencairan(pembiayaanReqDTO)).thenReturn(summaryHeaderPencairanResDTOs);
//        String jsonStr = objectMapper.writeValueAsString(summaryHeaderPencairanResDTO);
//
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/summary-pencairan")
//                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);
//
//        mockMvc.perform(requestBuilder).andExpect(status().isOk());
//    }

    private Application mapApplication(){
        Application application = new Application();
        application.setApplicationNo("APPL001");
        application.setApplicationContractNo("01");
        application.setApplicationDate(new Date());
        application.setSkApplication("SKA001");
        application.setFkCustomer("CUS001");
        application.setSourceApplication("first_source_application");
        application.setOrderDate(new Date());
        application.setApplicationDate(new Date());
        application.setSurveyAppointmentDate(new Date());
        application.setOrderType("first_order_type");
        application.setFlagAccount(true);
        application.setAccountNoFom("first_account_no_fom");
        application.setBranchSurvey("first_branch_survey");
        application.setBranchSales("first_branch_sales");
        application.setInitialRecomendation("first_initial_recommendation");
        application.setSignPk(1);
        application.setKonsepType("first_concept_type");
        application.setObjectQty(1);
        application.setPropInsrType("dummy_prop_insr_type");
        application.setUnit(1);
        application.setApplicationContractNo("001");
        application.setApplicationLastStatus("dummy_last_status");
        application.setCreatedBy("admin");
        application.setCreatedDate(new Date());
        application.setLastModifiedDate(new Date());
        application.setLastModifiedBy("admin");
        application.setActive(true);
        application.setFlagIA(true);
        application.setDealerNote("dummy_notes");
        application.setOrderNo("A1P0000SLD2100045382");

        return application;
    }

    private Customer mapCustomer(){
        Customer customer = new Customer();
        customer.setSkCustomer("SKC001");
        customer.setCustomerId("CUS001");
        customer.setCustomerOid("CUO001");
        customer.setCustomerType("first_customer_type");
        customer.setNpwpAddress("dummy_mpwp_address");
        customer.setNpwpName("dummy_npwp_name");
        customer.setNpwpNo("dummy_npwp_no");
        customer.setNpwpType("first_npwp_type");
        customer.setFlagNpwp(true);
        customer.setFlagPkpSign("dummy_flag_pkp_sign");
        customer.setFlagGuarantor(true);
        customer.setActive(true);
        customer.setCreatedDate(new Date());
        customer.setCreatedBy("admin");
        customer.setLastModifiedBy("admin");
        customer.setLastModifiedDate(new Date());

        return customer;
    }

    private SubmitOrder mapSubmitOrder(){
        SubmitOrder submitOrder = new SubmitOrder();
        submitOrder.setOrderNo("A1P0000SLD2100045382");
        submitOrder.setOrderDate(new Date());
        submitOrder.setSubmitOrderId("SOID001");
        submitOrder.setActive(0);
        submitOrder.setCreatedBy("admin");
        submitOrder.setCreatedDate(new Date());
        submitOrder.setApplNoPayung("");
        submitOrder.setApplNoUnit("APPL001");
        submitOrder.setIdx(1);
        submitOrder.setPartnerType("SLD");
        submitOrder.setPic("user@ad1gate.com");
        submitOrder.setStatus("Draft");
        submitOrder.setGroupStatus("dummy_group_status");

        return submitOrder;
    }

}
