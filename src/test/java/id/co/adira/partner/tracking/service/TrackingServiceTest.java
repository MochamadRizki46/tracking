package id.co.adira.partner.tracking.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.tracking.dto.request.TrackingOrderHeaderReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderHistoryReqDTO;
import id.co.adira.partner.tracking.dto.request.TrackingOrderWebReqDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHeaderResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderHistoryResDTO;
import id.co.adira.partner.tracking.dto.responses.TrackingOrderSummaryResDTO;
import id.co.adira.partner.tracking.entity.*;
import id.co.adira.partner.tracking.entity.params.ParaObject;
import id.co.adira.partner.tracking.externalapi.TrackingExternalApi;
import id.co.adira.partner.tracking.repository.*;
import id.co.adira.partner.tracking.repository.params.ParaKecamatanRepository;
import id.co.adira.partner.tracking.repository.params.ParaKelurahanRepository;
import id.co.adira.partner.tracking.repository.params.ParaObjectModelRepository;
import id.co.adira.partner.tracking.repository.params.ParaObjectRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class TrackingServiceTest {

    @InjectMocks
    TrackingService trackingService;

    @Mock
    DistributedApplicationTrackingRepository applicationTrackingRepository;

    @Mock
    ApplicationRepository applicationRepository;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    SubmitOrderRepository submitOrderRepository;

    @Mock
    ParaKelurahanRepository paraKelurahanRepository;

    @Mock
    ParaKecamatanRepository paraKecamatanRepository;

    @Mock
    ParaObjectRepository paraObjectRepository;
    
    @Mock
    ParaObjectModelRepository paraObjectModelRepository;

    @Mock
    PurchaseOrderRepository poRepository;

    @Mock
    List<Tuple> tuples;


    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    TrackingExternalApi externalApi;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;


    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        MockitoAnnotations.initMocks(this);
      
        
        
    }

//    @Test
//    public void testGetTrackingOrderHeaderAndExpectDataExists(){
//        TrackingOrderHeaderReqDTO trackingOrderHeaderReqDTO = new TrackingOrderHeaderReqDTO("A1P0000SPD2200001649", "22000001091", "22000001091001");
//
//        Application dummyApplication = mapApplication();
//        Customer dummyCustomer = mapCustomer();
//        SubmitOrder dummySubmitOrder = mapSubmitOrder();
//
//        when(applicationRepository.findByNoApplication("22000001091")).thenReturn(dummyApplication);
//        when(customerRepository.findByCustomerId("CUS001")).thenReturn(dummyCustomer);
//        when(submitOrderRepository.findByOrderNo("A1P0000SPD2200001649")).thenReturn(dummySubmitOrder);
//        when(paraObjectRepository.findByObjectId("dummy_object")).thenReturn(new ParaObject());
//        when(paraObjectModelRepository.findByObjectId("dummy_object_model")).thenReturn("dummy_object_model");
//       
//
//        TrackingOrderHeaderResDTO response = trackingService.getTrackingOrderHeader(trackingOrderHeaderReqDTO);
//        assertEquals(response.getOrderNo(), "A1P0000SPD2200001649");
//    }

    @Test
    public void testGetHistoryTrackingSlsAndExpectDataExists(){
        TrackingOrderHistoryReqDTO trackingOrderHistoryReqDTO = new TrackingOrderHistoryReqDTO();
        trackingOrderHistoryReqDTO.setApplNo("A1S0000SLD2100045382");
        trackingOrderHistoryReqDTO.setTypeUser("1");

        List<TrackingOrderHistoryResDTO> trackingOrderHistoryResDTOS = new ArrayList<>();

        TrackingOrderHistoryResDTO trackingOrderHistoryResDTO = new TrackingOrderHistoryResDTO();
        trackingOrderHistoryResDTO.setOrderNo("A1P0000SLD2100045382");
        trackingOrderHistoryResDTO.setApplNoUnit("A1PU001");
        trackingOrderHistoryResDTO.setStatus("Draft");
        trackingOrderHistoryResDTO.setSequence(1);
        trackingOrderHistoryResDTO.setSubstatus("dummy_sub_status");
        trackingOrderHistoryResDTO.setKodeGroupStatus("MAP");

        trackingOrderHistoryResDTOS.add(trackingOrderHistoryResDTO);
        when(applicationTrackingRepository.findByApplUnitNo(trackingOrderHistoryReqDTO.getApplNo().substring(0, 11))).thenReturn(trackingOrderHistoryResDTOS);

        List<TrackingOrderHistoryResDTO> responses = trackingService.getTrackingOrderHistorySls(trackingOrderHistoryReqDTO);
        assertEquals(responses.size(), 1);
    }

    @Test
    public void testGetSummaryTrackingOrderAndExpectDataExists(){
        TrackingOrderWebReqDTO trackingOrderWebReqDTO = new TrackingOrderWebReqDTO();
        trackingOrderWebReqDTO.setUserId("user@ad1gate.com");
        trackingOrderWebReqDTO.setStartDate("2022-02-08");
        trackingOrderWebReqDTO.setEndDate("2022-02-11");
        trackingOrderWebReqDTO.setGroupStatus("");
        trackingOrderWebReqDTO.setUserType("1");

        when(applicationTrackingRepository.findSummaryDraftByDateAndUserId(trackingOrderWebReqDTO.getStartDate(), trackingOrderWebReqDTO.getEndDate(), trackingOrderWebReqDTO.getUserId())).thenReturn(new BigInteger(String.valueOf(1)));
        when(applicationTrackingRepository.findSummaryTrackingOrderSales(trackingOrderWebReqDTO.getStartDate(), trackingOrderWebReqDTO.getEndDate(), trackingOrderWebReqDTO.getUserId())).thenReturn(tuples);

        List<TrackingOrderSummaryResDTO> trackingOrderSummaryResDTOS1 = trackingService.getSummaryTrackingOrder(trackingOrderWebReqDTO);
        assertEquals(trackingOrderSummaryResDTOS1.size(), 1);
    }

    private SubmitUnit mapSubmitUnit(){
        SubmitUnit submitUnit = new SubmitUnit();
        submitUnit.setSubmitOrderId("SO001");
        submitUnit.setObjectBrand("OBR001");
        submitUnit.setObjectBrandDesc("dummy_object_brand");
        submitUnit.setObjectType("OBT001");
        submitUnit.setObjectTypeDesc("dummy_object_type");
        submitUnit.setObjectModel("OBM001");
        submitUnit.setObjectModelDesc("dummy_object_model");
        submitUnit.setObject("OBJ001");
        submitUnit.setObjectDesc("dummy_object");
        submitUnit.setTipeJaminan("first_tipe_jaminan");
        submitUnit.setJenisAsuransi("first_jenis_asuransi");
        submitUnit.setKotaDomisili("dummy_kota_domisili");
        submitUnit.setObjectPrice(new BigDecimal(2000000));
        submitUnit.setTenor(12);
        submitUnit.setRate(new BigDecimal(10));
        submitUnit.setDp(new BigDecimal(40000));
        submitUnit.setInstallmentAmt(new BigDecimal(50000));

        return submitUnit;
    }

    private ApplicationObject mapApplicationObject(){
        List<ApplicationSalesInfo> applicationSalesInfos = new ArrayList<>();
        applicationSalesInfos.add(mapApplicationSalesInfo());

        ApplicationObject applicationObject = new ApplicationObject();
        applicationObject.setSkApplicationObject("SKA001");
        applicationObject.setApplicationNoUnit("APPU001");
        applicationObject.setFinancingType("first_financing_type");
        applicationObject.setBussActivities("dummy_buss_activities");
        applicationObject.setBussActivitiesType("dummy_buss_activities_type");
        applicationObject.setGroupObject("first_group_object");
        applicationObject.setObject("dummy_object");
        applicationObject.setProductType("first_product_type");
        applicationObject.setBrandObject("first_brand_object");
        applicationObject.setObjectType("first_object_type");
        applicationObject.setObjectModel("dummy_object_model");
        applicationObject.setObjectUsed("dummy_object_used");
        applicationObject.setObjectPurpose("dummy_object_purpose");
        applicationObject.setGroupSales("first_group_sales");
        applicationObject.setOrderSource("dummy_order_source");
        applicationObject.setOrderSourceName("dummy_order_source_name");
        applicationObject.setFlagThirdParty(true);
        applicationObject.setThirdPartyType("first_third_party_type");
        applicationObject.setThirdPartyId("dummy_third_party_id");
        applicationObject.setWorkType("first_work_type");
        applicationObject.setSentraId("dummy_sentra_id");
        applicationObject.setUnitId("dummy_unit_id");
        applicationObject.setProgram("first_program");
        applicationObject.setRehabType("first_rehab_type");
        applicationObject.setReferenceNo("dummy_reference_no");
        applicationObject.setApplicationContractNo("dummy_application_contract_no");
        applicationObject.setInstallmentType("first_installment_type");
        applicationObject.setApplicationTop(1);
        applicationObject.setPaymentMethod("first_payment_method");
        applicationObject.setEffRate(new BigDecimal(10));
        applicationObject.setFlatRate(new BigDecimal(0));
        applicationObject.setObjectPrice(new BigDecimal(1000000));
        applicationObject.setKaroseriPrice(new BigDecimal(100000));
        applicationObject.setTotalAmt(new BigDecimal(1000000));
        applicationObject.setDpNet(new BigDecimal(100000));
        applicationObject.setInstallmentDeclineN(BigDecimal.ZERO);
        applicationObject.setPaymentPerYear(new BigDecimal(50000));
        applicationObject.setDpBranch(new BigDecimal(100000));
        applicationObject.setDpGross(BigDecimal.ZERO);
        applicationObject.setPrincipalAmt(new BigDecimal(10));
        applicationObject.setInstallmentAmt(new BigDecimal(100000));
        applicationObject.setApplicationSalesInfo(applicationSalesInfos);

        return applicationObject;
    }

    private ApplicationSalesInfo mapApplicationSalesInfo() {
        ApplicationSalesInfo applicationSalesInfo = new ApplicationSalesInfo();
        applicationSalesInfo.setSkObjekSales("SKOS001");
        applicationSalesInfo.setEmployeeId("EMP001");
        applicationSalesInfo.setSalesType("001");
        applicationSalesInfo.setEmployeeHeadId("EH001");
        applicationSalesInfo.setEmployeeJob("dummy_employee_job");
        applicationSalesInfo.setActive(true);
        applicationSalesInfo.setCreatedDate(new Date());
        applicationSalesInfo.setCreatedBy("admin");
        applicationSalesInfo.setLastModifiedBy("admin");
        applicationSalesInfo.setLastModifiedDate(new Date());
        applicationSalesInfo.setEmployeeHeadJob("dummy_employee_head_job");
        applicationSalesInfo.setEmployeeName("dummy_employee_name");
        applicationSalesInfo.setFkApplicationObject("FKO001");

        return applicationSalesInfo;
    }

    private Application mapApplication(){
        List<ApplicationObject> applicationObjectList = new ArrayList<>();
        applicationObjectList.add(mapApplicationObject());

        Application application = new Application();
        application.setApplicationNo("APPL001");
        application.setApplicationContractNo("01");
        application.setApplicationDate(new Date());
        application.setSkApplication("SKA001");
        application.setFkCustomer("CUS001");
        application.setSourceApplication("first_source_application");
        application.setOrderDate(new Date());
        application.setApplicationDate(new Date());
        application.setSurveyAppointmentDate(new Date());
        application.setOrderType("first_order_type");
        application.setFlagAccount(true);
        application.setAccountNoFom("first_account_no_fom");
        application.setBranchSurvey("first_branch_survey");
        application.setBranchSales("first_branch_sales");
        application.setInitialRecomendation("first_initial_recommendation");
        application.setSignPk(1);
        application.setKonsepType("first_concept_type");
        application.setObjectQty(1);
        application.setPropInsrType("dummy_prop_insr_type");
        application.setUnit(1);
        application.setApplicationContractNo("001");
        application.setApplicationLastStatus("dummy_last_status");
        application.setCreatedBy("admin");
        application.setCreatedDate(new Date());
        application.setLastModifiedDate(new Date());
        application.setLastModifiedBy("admin");
        application.setActive(true);
        application.setFlagIA(true);
        application.setDealerNote("dummy_notes");
        application.setOrderNo("A1P0000SPD2200001649");
        application.setApplicationObject(applicationObjectList);

        return application;
    }

    private CustomerPersonal mapCustomerPersonal(){
        CustomerPersonal customerPersonal = new CustomerPersonal();
        customerPersonal.setSkCustomerPersonal("SKP001");
        customerPersonal.setNoOfLiability(1);
        customerPersonal.setCustomerGroup("first_customer_group");
        customerPersonal.setEducation("SMA");
        customerPersonal.setIdentityType("KTP");
        customerPersonal.setIdentityNo("36700000000000000");
        customerPersonal.setIdentityFullName("dummy_identity_full_name");
        customerPersonal.setAliasName("dummy_alias");
        customerPersonal.setDateOfBirth(new Date());
        customerPersonal.setPlaceOfBirth("dummy_place");
        customerPersonal.setPlaceOfBirthCity("dummy_city");
        customerPersonal.setGender("male");
        customerPersonal.setIdentityDate(new Date());
        customerPersonal.setIdentityExpireDate(new Date());

        return customerPersonal;
    }

    private Customer mapCustomer(){
        Customer customer = new Customer();
        customer.setSkCustomer("SKC001");
        customer.setCustomerId("CUS001");
        customer.setCustomerOid("CUO001");
        customer.setCustomerType("PER");
        customer.setNpwpAddress("dummy_mpwp_address");
        customer.setNpwpName("dummy_npwp_name");
        customer.setNpwpNo("dummy_npwp_no");
        customer.setNpwpType("first_npwp_type");
        customer.setFlagNpwp(true);
        customer.setFlagPkpSign("dummy_flag_pkp_sign");
        customer.setFlagGuarantor(true);
        customer.setActive(true);
        customer.setCreatedDate(new Date());
        customer.setCreatedBy("admin");
        customer.setLastModifiedBy("admin");
        customer.setLastModifiedDate(new Date());
        customer.setCustomerPersonal(mapCustomerPersonal());

        return customer;
    }

    private SubmitOrder mapSubmitOrder(){
        SubmitOrder submitOrder = new SubmitOrder();
        submitOrder.setOrderNo("A1P0000SLD2100045382");
        submitOrder.setOrderDate(new Date());
        submitOrder.setSubmitOrderId("SOID001");
        submitOrder.setActive(0);
        submitOrder.setCreatedBy("admin");
        submitOrder.setCreatedDate(new Date());
        submitOrder.setApplNoPayung("");
        submitOrder.setApplNoUnit("APPL001");
        submitOrder.setIdx(1);
        submitOrder.setPartnerType("SLD");
        submitOrder.setPic("user@ad1gate.com");
        submitOrder.setStatus("Draft");
        submitOrder.setGroupStatus("dummy_group_status");
        submitOrder.setSubmitUnit(mapSubmitUnit());

        return submitOrder;
    }

}
